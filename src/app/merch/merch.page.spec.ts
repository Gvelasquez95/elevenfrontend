import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchPage } from './merch.page';

describe('MerchPage', () => {
  let component: MerchPage;
  let fixture: ComponentFixture<MerchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
