import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.page.html',
  styleUrls: ['./evento.page.scss'],
})
export class EventoPage implements OnInit {

  public option = 'slider';

  public listaSlides:any = [];

  public listaTimeLine:any = [];

  constructor() {
    
  }

  ngOnInit() {

    this.listaSlides = [
      {
        id: '',
        img: '',
        titulo: '',
        descripcion: ''
      }
    ]

    this.listaTimeLine = [
      {
        id: '',
        date: '',
        data: {
          logo: '',
          titulo: '',
          descripcion: '',
          likes: '',
          time: '',
        }
      }
    ]

  }

}
