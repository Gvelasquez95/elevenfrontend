import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, NavParams } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-participacion-cliente',
  templateUrl: './participacion-cliente.page.html',
  styleUrls: ['./participacion-cliente.page.scss'],
})
export class ParticipacionClientePage implements OnInit {


  public listaParticipaciones:any = [];
  public descripcion:string='';
  public monto_pagado:number=0;
  public monto_total:number=3696;

  chartLegend:boolean = true;
  chartColor1: any[] = [{backgroundColor:['#0f8eac','#E1F2F5']}]; 

  public doughnutChartLabels:string[] = [];
  public doughnutChartData:number[] = [];
  public doughnutChartType:string = 'doughnut';

  DonutOptions: any = {
    responsive: true,
    rotation: 1 * Math.PI,
    circumference: 1 * Math.PI,
    legend: {position: 'top', labels: { fontColor: 'black' }}
  };

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
    
  constructor(public modalCtrl: ModalController, public loadingCtrl:LoadingController, public http: HttpClient, 
    public navParams: NavParams) { }

  ngOnInit() {
    var idUser = this.navParams.get('idCliente');
    this.getDataCliente(idUser);

    // this.monto_pagado = 2264; 
    // this.doughnutChartData = [this.monto_pagado,this.monto_total-this.monto_pagado];
    // this.doughnutChartLabels = ['Monto pagado: $'+this.monto_pagado,'Monto por pagar: $'+(this.monto_total-this.monto_pagado)]
  }

  async getDataCliente(iduser:string){

    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-bk-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

    this.http.get('https://11millionproject.com/eleven_app/3bk/user3bk.php?mis_pagos&id='+iduser).subscribe(data => {
      this.listaParticipaciones = data;
      if(this.listaParticipaciones == null){
        this.loadingCtrl.dismiss();
      }else{
      for(var i = 0; i < this.listaParticipaciones.length; i++){
        this.doughnutChartData = [parseInt(this.listaParticipaciones[i].monto_total), 3696 - parseInt(this.listaParticipaciones[i].monto_total)];
        this.doughnutChartLabels = ['Monto pagado: ('+this.listaParticipaciones[i].monto_total+')', 'Monto restante: ('+ (3696 - parseInt(this.listaParticipaciones[i].monto_total)) +')']
      }
      loading.dismiss();
      }
    });

  } 

  close(){
    this.modalCtrl.dismiss();
  }
}
