import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import {ChartsModule} from 'ng4-charts';
import '../../../node_modules/chart.js/dist/Chart.bundle.min.js'; 
import { ParticipacionClientePage } from './participacion-cliente.page';

const routes: Routes = [
  {
    path: '',
    component: ParticipacionClientePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChartsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ParticipacionClientePage]
})
export class ParticipacionClientePageModule {}
