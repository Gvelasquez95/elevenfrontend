import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipacionClientePage } from './participacion-cliente.page';

describe('ParticipacionClientePage', () => {
  let component: ParticipacionClientePage;
  let fixture: ComponentFixture<ParticipacionClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
