import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.page.html',
  styleUrls: ['./notification-detail.page.scss'],
})
export class NotificationDetailPage implements OnInit {

  public idNotification:string="";
  public estado:string="";
  public status:string="";

  constructor(public navParams: NavParams, public modalCtrl: ModalController) { }

  ngOnInit() {
    this.idNotification = this.navParams.get('idNotification');
    this.estado = this.navParams.get('Estado');
    console.log(this.idNotification+" "+this.estado);
    
    if(this.estado == '0'){
      this.status = '#FF6767';
    }else if(this.estado == '1'){
      this.status = '#E5E5E5';
    }else if(this.estado == '2'){
      this.status = '#FFC567';
    }else if(this.estado == '3'){
      this.status = '#76EE79';
    }
  }

  close(){
    this.modalCtrl.dismiss();
  }

}
