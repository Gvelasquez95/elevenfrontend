import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  myApiURL = 'https://elevenmillionproject.com/eleven_app/';
  tokenSend = '';

  constructor(public http: HttpClient) { }

  getMyApi(){
    return this.myApiURL;
  }

  getToken(){
    return btoa(localStorage.getItem('jwt'));
  }

  //Funciones
  login(data): Observable<any>{
    return this.http.post<any>(this.myApiURL+ 'userCtrl.php?log', {jwt:data},)
      .pipe(
        tap(_ => this.log('login')),
        catchError(this.handleError('login', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

}
