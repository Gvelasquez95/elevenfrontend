import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenCreditoPage } from './resumen-credito.page';

describe('ResumenCreditoPage', () => {
  let component: ResumenCreditoPage;
  let fixture: ComponentFixture<ResumenCreditoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenCreditoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenCreditoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
