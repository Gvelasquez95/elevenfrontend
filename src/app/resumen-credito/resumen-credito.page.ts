import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-resumen-credito',
  templateUrl: './resumen-credito.page.html',
  styleUrls: ['./resumen-credito.page.scss'],
})
export class ResumenCreditoPage implements OnInit {

  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  public close(){
    this.modalCtrl.dismiss();
  }

}
