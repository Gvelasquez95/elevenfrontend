import { Component, OnInit } from '@angular/core';
import { AlertController, PopoverController, ModalController, NavParams, LoadingController, ToastController, Events } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { IndexService } from '../../service/index.service';

@Component({
  selector: 'app-buzon-menu',
  templateUrl: './buzon-menu.component.html',
  styleUrls: ['./buzon-menu.component.scss'],
})
export class BuzonMenuComponent implements OnInit {

  public idNotification:string="";
  public estado:string="";
  public idUser:string="";

  constructor(public alertCtrl: AlertController, public popoverCtrl: PopoverController, public modalCtrl: ModalController,public toastCtrl: ToastController, 
    public navParams: NavParams, public http: HttpClient, private Storage: Storage, public loadingCtrl: LoadingController, public event: Events,
    public router: Router, public urI:IndexService) { }

  ngOnInit() {
    this.Storage.get('idUserEleven').then( data => {
      this.idUser = data;
    })
    this.idNotification = this.navParams.get('idNotification');
    this.estado = this.navParams.get('Estado');
  }

  async eliminate(){
    const alert = await this.alertCtrl.create(
      {
        backdropDismiss: false,
        translucent: false,
        header: 'Eliminar',
        message: '¿Esta seguro que desea eliminar esta solicitud? Una vez eliminada esta solicitud de pago, ya no podra recuperar la información.',
        buttons: 
        [
          {
            text: 'Cancelar',
            handler: () => {
              alert.dismiss();
            }  
          },
          {
            text: 'Eliminar',
            role: 'desctructive',
            handler: () => {
              this.eliminar()
            }  
          }
        ]
      }
    )
    await alert.present();
    this.popoverCtrl.dismiss();
  }

  async eliminar(){
    var url = this.urI.baseUrl()+'3bk/user3bk.php?delete_sol';

    var fe = new FormData();
    fe.append('idsol',this.idNotification);
    fe.append('iduser',this.idUser);
  
    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-eleven-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

      this.http.post(url, fe).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data || null ));
        console.log(result);
      
      let mensaje=result.status;
        console.log(mensaje);
        
      if(mensaje == "success"){
        this.makeText("Su solicitud de pago ha sido eliminada.");
        this.router.navigate(['/menu/negocio/triplebank/home']);
        this.modalCtrl.dismiss();
      }else{
        this.makeText(mensaje);
      }
        this.loadingCtrl.dismiss();
      },err => {
        this.loadingCtrl.dismiss();
      console.log(err);
      });
  }

  async makeText(mensaje){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 1500
    });
    await toast.present();
  }

}
