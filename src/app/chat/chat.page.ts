import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  public listaPilares:any = [];
  public listaChatPilares:any = [];
  public listaChatPersonal:any = [];
  option = 'pilares';

  constructor(public router: Router) { }

  ngOnInit() {
    this.listaPilares = [
      {
        id: '1',
        img: 'http://static.t13.cl/images/sizes/1200x675/1555883923-plantilla-foto-principal-notas.jpg'
      },
     
    ]

    this.listaChatPilares = [
      {
        id: '1',
        img: 'http://static.t13.cl/images/sizes/1200x675/1555883923-plantilla-foto-principal-notas.jpg',
        nombre: 'Gabriel Jimenes',
        last_message: '¿Que onda perro?',
        time: 'hoy'
      },
    ]

    this.listaChatPersonal = [
      {
        id: '1',
        img: 'http://static.t13.cl/images/sizes/1200x675/1555883923-plantilla-foto-principal-notas.jpg',
        nombre: 'Gabriel Jimenes',
        last_message: '¿Que onda perro?',
        time: 'hoy'
      },
    ]
  }

  public segmentChanged(ev:any){
    this.option = ev.detail.value;
    console.log(ev);
  }

  public goChat(codigo){
    this.router.navigate(['menu/chat/detalle', {id:codigo}]);
  }

}
