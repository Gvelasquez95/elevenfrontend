import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {

  public timer:string="";

  // public timeInSeconds;
  // public time;
  // public runTimer;
  // public hasStarted;
  // public hasFinished;
  // public remainingTime;
  // public displayTime;

  constructor(public router: Router, private storage: Storage) { }

  ngOnInit() {
    // this.initTimer();
    // this.startTimer();
    this.sesion();
  }
  
  // initTimer() {
  //    // Pomodoro is usually for 25 minutes
  //   if (!this.timeInSeconds) { 
  //     this.timeInSeconds = 3; 
  //   }
  
  //   this.time = this.timeInSeconds;
  //   this.runTimer = false;
  //   this.hasStarted = false;
  //   this.hasFinished = false;
  //   this.remainingTime = this.timeInSeconds;
    
  //   this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
    
    
  // }
  
  // startTimer() {
  //   this.runTimer = true;
  //   this.hasStarted = true;
  //   this.timerTick();
  // }
  
  // pauseTimer() {
  //   this.runTimer = false;
  // }
  
  // resumeTimer() {
  //   this.startTimer();
  // }
  
  // timerTick() {
  //   setTimeout(() => {
  
  //     if (!this.runTimer) { return; }
  //     this.remainingTime--;
  //     this.displayTime = this.getSecondsAsDigitalClock(this.remainingTime);
  //     console.log(this.displayTime);
  //     if (this.remainingTime > 0) {
  //       this.timerTick();
  //     }
  //     else {
  //       this.hasFinished = true;
  //       this.router.navigate(['/login']);
  //     }
  //   }, 950);
  // }
  
  // getSecondsAsDigitalClock(inputSeconds: number) {
  //   var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
  //   var hours = Math.floor(sec_num / 3600);
  //   var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  //   var seconds = sec_num - (hours * 3600) - (minutes * 60);
  //   var hoursString = '';
  //   var minutesString = '';
  //   var secondsString = '';
  //   hoursString = (hours < 10) ? "0" + hours : hours.toString();
  //   minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
  //   secondsString = (seconds < 10) ? "" + seconds : seconds.toString();
  //   return secondsString;
  // }

  public sesion(){
    this.storage.get('isChecked').then((val) => {
      if (val==true) {
        this.storage.get('idUserEleven').then((val) => {
          if (val!=null) {
            console.log(val);
            setTimeout(() => this.router.navigate(['/menu/home']), 3000);
          }else{
            console.log(val);
            setTimeout(() => this.router.navigate(['/login']), 3000);
          }
        });
      }else{
        console.log(val);
        setTimeout(() => this.router.navigate(['/login']), 3000);
      }
    });
  }

}
