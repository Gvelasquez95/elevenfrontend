import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController, LoadingController, AlertController, ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { ElevenRestService } from '../service/eleven-rest.service';
import { TerminosPage } from '../terminos/terminos.page';
import { IndexService } from '../service/index.service';
// import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public form = 'login'
  public show_login = 0;

  //userData:iLoginPageComponent={};
  public usuario:string;
  public clave:string;
  
  public ischecked:boolean=true;

  constructor(public router: Router, public navCtrl: NavController, public toastCtrl:ToastController,public alertCtrl: AlertController, 
    public loadingCtrl: LoadingController, private http: HttpClient, private storage: Storage, public elevenApi: ElevenRestService,public urI: IndexService,
    public modalCtrl:ModalController) {

    // this.userData.usuario = "";
    // this.userData.clave = "";
  }

  ionViewWillEnter(){
    this.clearInput();
  }

  ngOnInit(){
    this.show_login = 0;
    this.storage.get('terms').then((val) => {
     let value = val
     if(value != 1){
      setTimeout(() => {
        //this.terminos();
      },1500)
     }else{
       //nothing
     }
    });
    
  }
age(){
    this.router.navigate(['/signup']);
  }

    showLogin(){
      this.form = 'login';
      this.show_login = this.show_login === 0 ? 1 : 0;
    }

    showSignup(){
      this.form = this.form === 'register' ? 'login' : 'register';
    }


  async login(){

    var fl = new FormData();
    fl.append('user',this.usuario);
    fl.append('clave', this.clave);
    
    console.log(this.usuario +this.clave);
    
    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-eleven-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

    this.elevenApi.login(fl).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data || null ));
      let mensaje=result.status;
      
      if(mensaje == "success"){
        this.http.get(this.urI.baseUrl()+"userCtrl.php?login&user="+this.usuario+"&clave="+this.clave).subscribe( (data) => {
          let datainfo:any = data;

          for (let i = 0; i < datainfo.length; i++) {
            
            this.makeText("Bienvenido(a) a ElevenA Million App");
              this.router.navigate(['/menu/home']);
              this.storage.set('user',this.usuario);
              this.storage.set('pass',this.clave);
              this.storage.set('idUserEleven',datainfo[i].iduser);
              this.storage.set('dniUserEleven',datainfo[i].dni);
              var codeHash:string = datainfo[i].code;
              var code:string = codeHash.slice(1,11);
              this.storage.set('codeEleven',code)
              this.storage.set('isChecked',this.ischecked);
          }
        })
      }else{
        this.makeText(mensaje);
      }
        this.loadingCtrl.dismiss();
      },err => {
        this.loadingCtrl.dismiss();
      console.log(err);
      });
        
  }


  async alert(titulo,mensaje){
    const alert = await this.alertCtrl.create(
      {
        header: titulo,
        message: mensaje,
        backdropDismiss: true,
        buttons: [
          {
            text: 'Cerrar',
            role: 'destructive',
            handler: () =>{
              alert.dismiss();
          }
        }]
      });
      await alert.present();  
  }

  async loading(mensaje){
    const load = await this.loadingCtrl.create(
      {
        message: mensaje,
        spinner: "bubbles",
        mode: "ios",
        animated: true,
        backdropDismiss: false,
        showBackdrop: true
      })
    
    await load.present();
  }
  
  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 1500
      }
    )
    await toast.present()
  }

  async terminos(){
    const modal = await this.modalCtrl.create({
      component: TerminosPage,
      mode: 'ios',
      cssClass: 'terminosModal animated bounce',
      backdropDismiss: false
    });
    return await modal.present();
  }

  public clearInput(){
    this.usuario = "";
    this.clave = "";
  }

}
