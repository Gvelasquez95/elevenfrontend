/* Incluir Custom_elements_schema para habilitar elementos que no son de Angular  */
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';

/*Instalador de Video en BackGround*/
/*npm install --save gl-ionic-background-video*/

/* Modulo que se debe de importar a la pagina donde se desea colocar el video de fondo */
import 'gl-ionic-background-video';

const routes: Routes = [
  { path: '', component: LoginPage }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA] // Aqui se agrega SCHEMA
})
export class LoginPageModule {}
