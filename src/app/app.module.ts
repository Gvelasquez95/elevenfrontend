import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { PayPal } from '@ionic-native/paypal/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ChartsModule } from 'ng4-charts/ng4-charts';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { RegisterModalPageModule } from './register-modal/register-modal.module';
import { ElevenInvitePageModule } from './eleven-invite/eleven-invite.module';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';

//My components
import { MicSheetComponent } from './mic-sheet/mic-sheet.component';
import { PaymentsPageModule } from './payments/payments.module';
import { PaymentDetailPageModule } from './payment-detail/payment-detail.module';
import { GananciadetailboxPageModule } from './gananciadetailbox/gananciadetailbox.module';
import { BuzonMenuComponent } from './mycomponents/buzon-menu/buzon-menu.component';
import { NotificationDetailPageModule } from './notification-detail/notification-detail.module';
import { PagoAdelantadoPageModule } from './pago-adelantado/pago-adelantado.module';
import { ParticipacionClientePageModule } from './participacion-cliente/participacion-cliente.module';
import { HistorialCreditoPageModule } from './historial-credito/historial-credito.module';
import { TerminosPageModule } from './terminos/terminos.module';
import { ResumenCreditoPageModule } from './resumen-credito/resumen-credito.module';
import { CulqiService } from './service/culqi.service';
import { PaymentCartPageModule } from './payment-cart/payment-cart.module';
import { WalletModalPageModule } from './wallet-modal/wallet-modal.module';

import { IndexService } from './service/index.service';
import { ExtrasService } from './service/extras.service';




@NgModule({
  declarations: [AppComponent, MicSheetComponent, BuzonMenuComponent],
  entryComponents: [MicSheetComponent, BuzonMenuComponent],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
    ChartsModule,
    RegisterModalPageModule,
    ElevenInvitePageModule,
    PaymentsPageModule,
    PaymentDetailPageModule,
    GananciadetailboxPageModule,
    NotificationDetailPageModule,
    PagoAdelantadoPageModule,
    ParticipacionClientePageModule,
    HistorialCreditoPageModule,
    TerminosPageModule,
    ResumenCreditoPageModule,
    WalletModalPageModule,
    PaymentCartPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PayPal,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    InAppBrowser,
    FileChooser,
    FileTransfer,
    FilePath,
    Base64,
    SpeechRecognition,
    TextToSpeech,
    AndroidPermissions,
    SocialSharing,
    HTTP,
    CallNumber,
    CulqiService,
    IndexService,
    ExtrasService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
