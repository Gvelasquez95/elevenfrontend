import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GananciaPage } from './ganancia.page';

describe('GananciaPage', () => {
  let component: GananciaPage;
  let fixture: ComponentFixture<GananciaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GananciaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GananciaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
