import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import { IonSlides } from '@ionic/angular';
import { IndexService } from 'src/app/service/index.service';

@Component({
  selector: 'app-ganancia',
  templateUrl: './ganancia.page.html',
  styleUrls: ['./ganancia.page.scss'],
})
export class GananciaPage implements OnInit {
  
  @ViewChild('slider') slides: IonSlides;

  public iduser:string="";
  public dniUser:string="";
  public dataGanancia:any = [];
  public redData:any= [];

  //Data
  public clientes:number=0;
  public red:number=0;

  public total_inscripcion:string="";
  public total_membresia:string="";
  public ganancia_bruta;
  public ganancia_neta;

  chartLegend:boolean = true;
  chartColor1: any[] = [{backgroundColor:['#0f8eac','#E1F2F5']}]; 

  public doughnutChartLabels:string[] = ['Miembros logrados #', 'Miembros restantes #'];
  public doughnutChartData:number[] = [];
  public doughnutChartType:string = 'doughnut';

  public pieChartLabels:string[] = ['Clientes','Referidos'];
  public pieChartData:number[] = [];
  public pieChartType:string = 'pie';

  chartColor2: any[] = [{backgroundColor:['#0f8eac','#6CB6C3','#E1F2F5']}]; 
  public lineChartLabels:string[] = ['Ganancia por facturar','Ganancia por cobrar', 'Ganancia por inscripciones'];
  public lineChartData:number[] = [];
  public lineChartType:string = 'horizontalBar';

  public currentIndex:number=0;

  // events
  public chartClicked(e:any):void {
    
  }

  public chartHovered(e:any):void {
    
  }
    
  halfDonutOptions: any = {
    responsive: true,
    rotation: 1 * Math.PI,
    circumference: 1 * Math.PI,
    legend: {position: 'bottom', labels: { fontColor: 'black' }}
  };

  pieOptions: any = {
    responsive: true,
    legend: {position: 'bottom', labels: { fontColor: 'black' }}
  };

  lineChartOptions: any = {
    responsive: true,
    scaleShowVerticalLines: true,
    legend: {
      display: false,
    }
  };

  slideOpts = {
    initialSlide: 0,
    speed: 1500,
    autoplay: true,
    zoom: {
      maxRatio: 1
    }
  };

  constructor(public router: Router, public http: HttpClient, private storage: Storage, public urI: IndexService) { }

  ngOnInit(){
    this.storage.get('idUserEleven').then(data => {
      this.iduser = data;
      this.storage.get('dniUserEleven').then(dni => {
        this.dniUser = dni;
        this.getData(this.iduser,this.dniUser);
      });
      
    });
  }

   getData(user,dni){
    this.http.get(this.urI.baseUrl()+'3bk/user3bk.php?ganancia_user&user='+user).subscribe( (data) => {
      this.dataGanancia = data;
      for(let data of this.dataGanancia){
        this.clientes = data.total_cliente;
        this.total_inscripcion = data.total_inscripcion;
        console.log(this.urI.baseUrl()+'3bk/user3bk.php?ganancia_user&user='+user);
        
        this.doughnutChartData = [this.clientes, (1000-this.clientes)];
        
        this.http.get(this.urI.baseUrl()+'3bk/3bk.php?referidos&dni='+dni).subscribe( (data) => {
          this.redData = data;
          for(let d of this.redData){
            this.red = this.redData.length;
            this.pieChartData = [this.clientes, this.red];
            
            this.http.get(this.urI.baseUrl()+"3bk/3bk.php?referidos_pago&dni="+dni).subscribe( (data) => {
              let dataInfo:any = data;
              for(let data of dataInfo){
              this.ganancia_bruta = data.dolares;
              this.ganancia_bruta = parseFloat(this.ganancia_bruta);
              this.ganancia_bruta = (Math.round(this.ganancia_bruta * 100) / 100);
      
              this.ganancia_neta = data.fin;
              this.ganancia_neta = parseFloat(this.ganancia_neta);
              this.ganancia_neta = (Math.round(this.ganancia_neta * 100) / 100);
      
              this.lineChartData = [this.ganancia_bruta,this.ganancia_neta,this.total_inscripcion];
              
            }
          });

          }
        });

      }
    })
  }

  public goPage(pagina){
    this.router.navigate(['/menu/negocio/triplebank/'+pagina]);
  }

  public func(ev:any){
    this.slides.getActiveIndex().then(index => {
      this.currentIndex = index;
    });
  }

}
