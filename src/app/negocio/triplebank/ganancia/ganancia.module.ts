import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import {ChartsModule} from 'ng4-charts';
import '../../../../../node_modules/chart.js/dist/Chart.bundle.min.js'; 

import { GananciaPage } from './ganancia.page';

const routes: Routes = [
  {
    path: '',
    component: GananciaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChartsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GananciaPage]
})
export class GananciaPageModule {}
