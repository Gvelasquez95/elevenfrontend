import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoadingController, PopoverController, ModalController, Events } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { BuzonMenuComponent } from 'src/app/mycomponents/buzon-menu/buzon-menu.component';
import { NotificationDetailPage } from 'src/app/notification-detail/notification-detail.page';
import { IndexService } from 'src/app/service/index.service';

@Component({
  selector: 'app-buzon',
  templateUrl: './buzon.page.html',
  styleUrls: ['./buzon.page.scss'],
})
export class BuzonPage implements OnInit {

  public detail;
  public listaNotificacion:any = [];
  public lista2:any = [];
  public status;
  public search;
  public leyenda;
  public filterColor;

  constructor(public router: Router, public http: HttpClient, public loadingCtrl: LoadingController, private storage: Storage,public urI:IndexService,
    public popoverCtrl: PopoverController, public modalCtrl: ModalController, public events: Events) {
    this.search = '0';
    this.leyenda = 'Leyenda';
    this.filterColor = '#0087A7';
  }

  ionViewWillEnter(){
    this.storage.get('idUserEleven').then(data => {
      this.getNotificaciones(data);
    });
  }

  ngOnInit(){
    this.storage.get('idUserEleven').then(data => {
      this.getNotificaciones(data);
    });
  }

  showDetail(){
    this.detail = this.detail === '0' ? '1' : '0';
  }

  showAll(){
    this.lista2 = this.listaNotificacion;
    this.status = '4';
    this.leyenda = 'Leyenda';
    this.filterColor = '#0087A7';
  }

  async getNotificaciones(user){

    var loading = await this.loadingCtrl.create({
      spinner: null,
        cssClass: 'custom-eleven-loading',
        backdropDismiss: false
    });
    loading.present()

    this.http.get(this.urI.baseUrl()+'3bk/3bk.php?notificaciones&user='+user).subscribe( data => {
      var listado:any = data;
      for(let d of listado){
        this.listaNotificacion = d.datos;
        console.log(this.listaNotificacion);
        this.lista2 = this.listaNotificacion;
        loading.dismiss();
      } 
    })

  }

  public filter(state){
    this.status = state;
    this.lista2 = this.listaNotificacion;
    let lista = this.lista2.filter(x => x.estado === state);
    if(state == '0'){
      this.leyenda = 'Denegado';
      this.filterColor = '#FF6767';
    }else if(state == '1'){
      this.leyenda = 'En proceso';
      this.filterColor = '#E5E5E5';
    }else if(state == '2'){
      this.leyenda = 'Aprobado';
      this.filterColor = '#FFC567';
    }else if(state == '3'){
      this.leyenda = 'Pagado';
      this.filterColor = '#76EE79';
    }
    console.log(lista);
    this.lista2 = lista;
  }

  public goBack(){
    this.router.navigate(['/menu/negocio/triplebank/tabs/tabs/cobrar']);
  }

  public showSB(){
    this.search = this.search === '0' ? '1' : '0';
  }

  async detalle(id,estado){
    const modal = await this.modalCtrl.create({
      component: NotificationDetailPage,
      componentProps: { 
        idNotification: id,
        Estado: estado
      },
      mode: 'ios',
      cssClass: 'membresiaModal animated bounce,',
      backdropDismiss: false
    });
    this.popoverCtrl.dismiss();
    return await modal.present();
  }

  async extraOption(ev: any,id,estado) {
    const popover = await this.popoverCtrl.create({
      component: BuzonMenuComponent,
      event: ev,
      translucent: false,
      backdropDismiss: true,
      componentProps: { idNotification: id, Estado: estado }
    });
    await popover.present();
  }

}
