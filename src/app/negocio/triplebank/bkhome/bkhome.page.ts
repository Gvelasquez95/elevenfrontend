import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, NavController, ToastController, LoadingController ,Events, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
// import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { HttpClient } from '@angular/common/http';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

import { RegisterModalPage } from 'src/app/register-modal/register-modal.page';
import { PaymentsPage } from 'src/app/payments/payments.page';
import { GananciadetailboxPage } from 'src/app/gananciadetailbox/gananciadetailbox.page';
import { PagoAdelantadoPage } from 'src/app/pago-adelantado/pago-adelantado.page';
import { ResumenCreditoPage } from 'src/app/resumen-credito/resumen-credito.page';
import { PaymentCartPage } from 'src/app/payment-cart/payment-cart.page';
import { IndexService } from 'src/app/service/index.service';
import { WalletModalPage } from 'src/app/wallet-modal/wallet-modal.page';

@Component({
  selector: 'app-bkhome',
  templateUrl: './bkhome.page.html',
  styleUrls: ['./bkhome.page.scss'],
})
export class BkhomePage implements OnInit {

  public listaLotes:any = "";
  public iduser:string="";
  public dniUser:string="";
  public usuario="";public clave="";public tipo_pago="";
  public monto:number=0;public cuotas="";public description="";
  public total="";public lote="";
  public listaParticipacion:any=[];
  
  //Detalles
  public ganancia_bruta;
  public ganancia_neta;

  public descuento3bk;
  public descuentopaypal;

  //Parametros
  fotoUser;nombreUser;lanzador;

  customSelect: any = {
    header: 'Seleccione cuotas',
    translucent: true
  };

  constructor(public router: Router, public modalCtrl: ModalController, public activatedRoute: ActivatedRoute,public toastCtrl: ToastController,public events:Events,public urI:IndexService,
    public navCtrl: NavController, public storage: Storage, public http: HttpClient, public iab: InAppBrowser, public loadingCtrl: LoadingController,public alertCtrl: AlertController) {
 
  }
  ionViewWillEnter(){
    this.initialize();
  }
  ngOnInit() {
    //this.pagoAdelantado();
   // this.initialize();

  }

  public getIngresos(dni){
    this.http.get(this.urI.baseUrl()+"3bk/3bk.php?referidos_pago&dni="+dni).subscribe( (data) => {
      let dataInfo:any = data;
      console.log("dataIngresos: "+dataInfo);
      for(let data of dataInfo){
        this.ganancia_bruta = data.dolares;
        this.ganancia_bruta = parseFloat(this.ganancia_bruta);
        this.ganancia_bruta = (Math.round(this.ganancia_bruta * 100) / 100);

        this.descuento3bk = parseFloat(this.ganancia_bruta);
        this.descuento3bk = ((this.ganancia_bruta * 10 ) / 100);
        this.descuento3bk = (Math.round(this.descuento3bk * 100) / 100);

        let resta1:any = parseFloat(this.ganancia_bruta) - parseFloat(this.descuento3bk); 
        resta1 = (Math.round(resta1 * 100) / 100);

        let descpp:any = parseFloat(resta1);
        descpp = ((descpp * 15 ) / 100);
        descpp = (Math.round(descpp * 100) / 100);

        let resta2:any = parseFloat(resta1) - parseFloat(descpp);
        resta2 = (Math.round(resta2 * 100) / 100);
    
        this.ganancia_neta = parseFloat(resta2);
        this.ganancia_neta = (Math.round(this.ganancia_neta * 100) / 100);

      }
    });
  }

  // public getLotes(id){
  //   this.http.get("https://elevenmillionproject.com/eleven_app/3bk/3bk.php?list_part&user="+id).subscribe( (data) => {
  //     this.listaLotes = data;
  //   });
  // }

  public getDataUser(user,pass){
    this.http.get(this.urI.baseUrl()+"3bk/user3bk.php?login&user="+user+"&clave="+pass).subscribe( (data) => {
      let datainfo:any = data;
      console.log(datainfo);
      for(let datos of datainfo){
        this.fotoUser = datos.foto;
        this.nombreUser = datos.fullname;
        this.lanzador = datos.lanzador;
        this.iduser = datos.iduser;
        // this.getLotes(this.iduser);
      }
    }); 
  }

  gowebPaypal(){
    const options : InAppBrowserOptions = {
      zoom: "no"
    }

    const browser = this.iab.create('https://3bk.11millionproject.com/pago_sky.php?cerrar', '_self', options);
  }

      async registerModal(){
        const modal = await this.modalCtrl.create({
          component: RegisterModalPage,
          componentProps: { 
            idUser: this.iduser
          },
          mode: 'ios',
          cssClass: 'registerModal animated bounce',
          backdropDismiss: true
        });
        return await modal.present();
      }

      async paymentsModal(){
        const modal = await this.modalCtrl.create({
          component: PaymentsPage,
          componentProps: { 
            id: this.iduser
          },
          mode: 'ios',
          cssClass: 'membresiaModal animated zoomIn',
          backdropDismiss: false
        });
        return await modal.present();
      }

      async gananciadetailbox(){
        const modal = await this.modalCtrl.create({
          component: GananciadetailboxPage,
          componentProps: { 
            bruto: this.ganancia_bruta,
            desc3bk: this.descuento3bk,
            descpp: this.descuentopaypal,
            neto: this.ganancia_neta
          },
          mode: 'ios',
          cssClass: 'gananciadetailModal animated fadeInDownBig',
          backdropDismiss: true
        });
        return await modal.present();
      }

      async pagoAdelantado(){
        const modal = await this.modalCtrl.create({
          component: PagoAdelantadoPage,
          componentProps: { 
            idUser: this.iduser
          },
          mode: 'ios',
          cssClass: 'pagoAdelantado animated flipInY',
          backdropDismiss: true
        });
        return await modal.present();
      }

      async historialModal(){
        const modal = await this.modalCtrl.create({
          component: ResumenCreditoPage,
          componentProps: { 
            idUser: this.iduser
          },
          mode: 'ios',
          cssClass: 'membresiaModal animated flipInY',
          backdropDismiss: true
        });
        return await modal.present();
      }

      async goCartPayment(){
        const modal = await this.modalCtrl.create({
          component: PaymentCartPage,
          componentProps: { 
            idUser: this.iduser
          },
          mode: 'ios',
          cssClass: 'walletModal animated flipInY',
          backdropDismiss: false
        });
        return await modal.present();
      }

      // async goWalletModal(){
      //   const modal = await this.modalCtrl.create({
      //     component: WalletModalPage,
      //     componentProps: { 
      //       idUser: this.iduser
      //     },
      //     mode: 'ios',
      //     cssClass: 'walletModal animated flipInY',
      //     backdropDismiss: false
      //   });
      //   return await modal.present();
      // }

      goBack(){
        this.storage.remove('idUser3bk');
        this.storage.set('idUser3bk','');
        this.navCtrl.navigateBack('/menu/negocio/triplebank');
      }

      
      goPage(pagina){
        this.router.navigate(['/menu/negocio/triplebank/'+pagina]);
      }
      
      async makeText(mensaje:string){
        const toast = await this.toastCtrl.create({
          message : mensaje,
          duration: 2500
        });
        toast.present();
      }
    
    async initialize(){

      let loading = await this.loadingCtrl.create(
        {
          spinner: null,
          cssClass: 'custom-bk-loading',
          backdropDismiss: false
        }
      );
      await loading.present();

      this.storage.get('codeEleven').then(u => {
        this.usuario = u;
        this.storage.get('dniUserEleven').then(p => {
          this.clave = p;
            this.getDataUser(this.usuario,this.clave);
        });
      });

      this.storage.get('idUserEleven').then(data => {
        this.iduser = data;
        this.getListaParticipacion(this.iduser);
      })
  
      this.storage.get('dniUserEleven').then(dni => {
        this.dniUser = dni;
        console.log("dniUser: "+this.dniUser);
        this.getIngresos(this.dniUser);
        this.loadingCtrl.dismiss();
      });

    }

    async getListaParticipacion(idUser){  
      this.http.get('https://11millionproject.com/eleven_app/3bk/user3bk.php?mis_pagos&id='+idUser).subscribe( data => {
        this.listaParticipacion = data;
        console.log(this.listaParticipacion.length);
      });
    }

    async checkParticipacion(){
      console.log(this.listaParticipacion.length);
      
      if(this.listaParticipacion == null){
        alert("No cuenta con participaciones activas, comuniquese con un asesor del proyecto SkyWay.")
      }else{
        this.goCartPayment();
      }
    }

}
