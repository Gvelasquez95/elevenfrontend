import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      { path: 'bkhome', loadChildren: '../bkhome/bkhome.module#BkhomePageModule' },
      { path: 'cliente', loadChildren: '../cliente/cliente.module#ClientePageModule' },
      { path: 'ganancia', loadChildren: '../ganancia/ganancia.module#GananciaPageModule' },
      { path: 'red', loadChildren: '../red/red.module#RedPageModule' },
      { path: 'cobrar', loadChildren: '../cobrar/cobrar.module#CobrarPageModule' },
      { path: 'buzon', loadChildren: '../buzon/buzon.module#BuzonPageModule' },
      { path: 'billetera', loadChildren: '../billetera/billetera.module#BilleteraPageModule' }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/bkhome',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
