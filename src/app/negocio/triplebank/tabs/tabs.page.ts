import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  public mode:string="";

  constructor(public router: Router) {
    this.mode='bkhome';
  }

  ngOnInit() {
  }

  public goPage(page){
    this.router.navigate(['/menu/negocio/triplebank/tabs/tabs/'+page])
  }

  public goRouter(ev:any){
    var link = ev.tab;
    if(link == 'bkhome'){
      this.mode = 'bkhome';
    }else if(link == 'billetera'){
      this.mode = 'billetera';
    }else if(link == 'cobrar'){
      this.mode = 'cobrar';
    }else if(link == 'cliente'){
      this.mode = 'cliente';
    }else if(link == 'ganancia'){
      this.mode = 'ganancia';
    }
    console.log("tab:" +ev.tab);
  }

  public backEleven(){
    this.router.navigate(['/menu/home']);
  }

}
