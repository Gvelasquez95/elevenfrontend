import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { LoadingController, ToastController, ModalController } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ParticipacionClientePage } from 'src/app/participacion-cliente/participacion-cliente.page';
import { IndexService } from 'src/app/service/index.service';


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.page.html',
  styleUrls: ['./cliente.page.scss'],
})
export class ClientePage implements OnInit {

  shownDetail = null;
  public listaClientes:any = []
  public listaCargadaClientes:any = [];

  public dniUser:string = "";
  public searchTerm:string = '';

  constructor(public http: HttpClient, public router: Router, private storage: Storage, public loadingCtrl: LoadingController,public urI: IndexService,
    public toastCtrl: ToastController, private callNumber: CallNumber, public modalCtrl: ModalController){ }

  ngOnInit() {
    this.storage.get('dniUserEleven').then(dni => {
      this.dniUser = dni;
      console.log("dniUser: "+this.dniUser);
      this.getListaClientes(this.dniUser);
    });
  }

  async getListaClientes(dni){

    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-bk-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

    this.http.get(this.urI.baseUrl()+'3bk/3bk.php?clientes&dni='+dni).subscribe( (data) => {
      this.listaClientes = data;
      this.listaCargadaClientes = this.listaClientes;
      loading.dismiss();
    })
  }

  public goPage(pagina){
    this.router.navigate(['/menu/negocio/triplebank/'+pagina]);
  }

  public toggleDetail(group){
    if (this.isDetailShown(group)) {
      this.shownDetail = null;
    } else {
        this.shownDetail = group;
    }
  }

  public isDetailShown(group) {
    return this.shownDetail === group;
  };

  searchList(): void {
    this.listaClientes = this.listaCargadaClientes;
    const query = (this.searchTerm && this.searchTerm !== null) ? this.searchTerm : '';
    this.listaClientes = this.filterList(this.listaCargadaClientes , query);
  }

  filterList(list, query): Array<any> {
    return list.filter(data => data.fullname.toLowerCase().includes(query.toLowerCase()));
  }

  async makeText(mensaje){
    const alert = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2000
      }
    );
    await alert.present();
  }

  public call(number){
    this.callNumber.callNumber(number, true)
    .then(res => console.log('¡Iniciando Llamada!', res))
    .catch(err => console.log('Error al iniciar llamada', err));
  }

  async participacionCliente(id){
    const modal = await this.modalCtrl.create({
      component: ParticipacionClientePage,
      componentProps: { 
        idCliente: id
      },
      mode: 'ios',
      cssClass: 'membresiaModal animated fadeInDownBig',
      backdropDismiss: true
    });
    return await modal.present();
  }

}
