import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController, LoadingController, MenuController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-triplebank',
  templateUrl: './triplebank.page.html',
  styleUrls: ['./triplebank.page.scss'],
})
export class TriplebankPage implements OnInit {

  public negocianteData:any = [];
  public idUser:string="";
  public usuario:string="";
  public clave:string="";

  constructor(public router: Router, public toastCtrl: ToastController, public loadingCtrl: LoadingController,
    public http: HttpClient, public menu: MenuController, private storage: Storage) { 
    this.menu.enable(false);
    //this.hideTabs();
  }

  ionViewWillEnter(){
    //this.hideTabs();
    this.menu.enable(false);
  }

  ngOnInit() {
    this.storage.get('idUserEleven').then(data => {
      this.idUser = data;
    })
  }

  async login(){

    var url="https://11millionproject.com/eleven_app/3bk/user3bk.php?log";

    var fl = new FormData();
    fl.append('user',this.usuario);
    fl.append('clave', this.clave);
    fl.append('iduser', this.idUser);
    
    console.log(this.usuario  +'u'+this.clave+'c'+this.idUser+'i');
    
    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-bk-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

      this.http.post(url, fl).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data || null ));
      let mensaje=result.status;
        console.log(mensaje);
        
      if(mensaje == "success"){
        this.http.get("https://11millionproject.com/eleven_app/3bk/user3bk.php?login&user="+this.usuario+"&clave="+this.clave).subscribe( (data) => {
          let datainfo:any = data;
          console.log(datainfo);
          for(let datos of datainfo){   
            this.makeText("Bienvenido(a) a TripleBank");
            this.router.navigate(['/menu/negocio/triplebank/tabs']);
            this.storage.set('user3bk',this.usuario);
            this.storage.set('pass3bk',this.clave);
            this.storage.set('idUser3bk',datos.iduser);
            this.loadingCtrl.dismiss();
          }
        })
      }else{
        this.makeText(mensaje);
      }
        this.loadingCtrl.dismiss();
      },err => {
        this.loadingCtrl.dismiss();
      console.log(err);
      });
  }

  async makeText(mensaje){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 1500
    });

    toast.present();
  }

  public register(){
    this.router.navigate(['/menu/negocio/triplebank/register']);
  }

  public hideTabs() {
    const tabBar = document.getElementById('elevenBar');
    if (tabBar.style.display !== 'none') tabBar.style.display = 'none';
  }

}
