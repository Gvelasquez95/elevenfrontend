import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriplebankPage } from './triplebank.page';

describe('TriplebankPage', () => {
  let component: TriplebankPage;
  let fixture: ComponentFixture<TriplebankPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriplebankPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriplebankPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
