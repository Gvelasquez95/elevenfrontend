import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { LoadingController, ToastController } from '@ionic/angular';
import { IndexService } from 'src/app/service/index.service';


@Component({
  selector: 'app-red',
  templateUrl: './red.page.html',
  styleUrls: ['./red.page.scss'],
})
export class RedPage implements OnInit {

  shownDetail = null;
  public listaClientes:any = []
  public listaCargadaClientes:any = [];

  public dniUser:string="";
  public searchTerm:string = '';

  constructor(public http: HttpClient, public router: Router, private storage: Storage, public loadingCtrl: LoadingController,public urI: IndexService,
    public toastCtrl:ToastController) { }

  ngOnInit() {
    this.storage.get('dniUserEleven').then(dni => {
      this.dniUser = dni;      
      this.getListaRed(this.dniUser);
    })
    
  }

  async getListaRed(dni){

    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-bk-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

    this.http.get(this.urI.baseUrl()+'3bk/3bk.php?referidos&dni='+dni).subscribe( (data) => {
      this.listaClientes = data;
      this.listaCargadaClientes = this.listaClientes;
      loading.dismiss();
      console.log(this.urI.baseUrl()+'3bk/3bk.php?referidos&dni='+dni);
    });
  }

  public goPage(pagina){
    this.router.navigate(['/menu/negocio/triplebank/'+pagina]);
  }

  public toggleDetail(group){
    if (this.isDetailShown(group)) {
      this.shownDetail = null;
    } else {
        this.shownDetail = group;
    }
  }

  public isDetailShown(group) {
    return this.shownDetail === group;
  };

  searchList(): void {
    this.listaClientes = this.listaCargadaClientes;
    const query = (this.searchTerm && this.searchTerm !== null) ? this.searchTerm : '';
    this.listaClientes = this.filterList(this.listaCargadaClientes , query);
  }

  filterList(list, query): Array<any> {
    return list.filter(data => data.fullname.toLowerCase().includes(query.toLowerCase()));
  }


  async makeText(mensaje){
    const alert = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 2000
      }
    );
    await alert.present();
  }

  goBack(){
    this.router.navigate(['/menu/negocio/triplebank/tabs/tabs/cliente'])
  }


}
