import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { IndexService } from 'src/app/service/index.service';
import { Storage } from '@ionic/storage';
import { ExtrasService } from 'src/app/service/extras.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

var data:any={};
var monto;
var description;
var tipo;
@Component({
  selector: 'app-billetera',
  templateUrl: './billetera.page.html',
  styleUrls: ['./billetera.page.scss'],
})
export class BilleteraPage implements OnInit {

  selectedOption:any;
  optionView:any;
  SubTitle:string="";
  estado:string=""; 

  public listaHistorial:any = [];
  public pagoPagos:any = [];

  public selectedPagos:any = [];
  public monto_total:any=0;
  public monto_acumulado:any=0;
  public allBoxes:string="Todo";
  isIndeterminate:boolean;
  masterCheck:boolean;

  shownDetail = null;
  public idUser:string="";
  // pagar con saldo
  public lista_cuotas:any=[];
  public listaParticipacion:any = [];
  public selectedItem:string="";
  public participacionData:any=[];
  public participacion:string="";
  public monto_por_pagar:number=0;
  public monto_por_pagar_url:number=1;
  public monto_a_pagar:number=0;
  public fecha_vence;
  public fecha_ultimo_pago;
  public checkedPay:boolean=false;
  public fecha_ultim_pago='';
  // fin
  constructor(private storage: Storage, public http: HttpClient, public urI: IndexService,
    public extras: ExtrasService, public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public router: Router) {
    this.storage.get('idUserEleven').then(value => {
      this.idUser = value;
      const headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Content-Type','application/json');
      headers.append('Accept','application/json');
      this.http.get(this.urI.baseUrl()+'3bk/wallet.php?historial&user='+this.idUser, {headers : headers}).subscribe(data => {
        // this.listaHistorial = data;

        this.monto_acumulado = data[0].saldo;
      })
      this.getListaParticipacion(this.idUser);
    })

   }

  ngOnInit() {
    this.listaHistorial=[];
    this.selectedItem="";
    this.lista_cuotas=[];
    this.selectedOption = 0;
    this.estado ="2"; 
  }

  public getHistorial(iduser){

    this.extras.loadingCharge('');

    let headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Content-Type','application/json');
    headers.append('Accept','application/json');

    this.http.get(this.urI.baseUrl()+'3bk/wallet.php?historial&user='+iduser, {headers : headers}).subscribe(data => {
      this.listaHistorial = data;
      this.monto_acumulado = data[0].saldo;
      this.loadingCtrl.dismiss();
    })

  }

  

  optionSelected(value){
    this.selectedOption = 1;
    
    if(value == 1){
      this.optionView = value;
      this.SubTitle = 'Historial'; 

      this.storage.get('idUserEleven').then(value => {
        this.idUser = value;
        this.getHistorial(this.idUser);
      });
    }else if(value == 2){
      this.optionView = value;
      this.SubTitle = 'Billetera'; 
    }else if(value == 3){
      this.selectedItem="";
      this.lista_cuotas=[];
      this.monto_a_pagar=0;

      this.optionView = value;
      this.SubTitle = 'Pagar participación'; 
    }else{
      this.optionView = value;
      this.SubTitle = 'Transferir'; 
    }
  }

  nonoptionSelected(){
    this.listaHistorial=[];
    this.selectedOption = 0;
    this.optionView = '';
    this.SubTitle = ''; 
  }

  // pagar con cuotas
  async getListaParticipacion(idUser){

    const loading = await this.loadingCtrl.create({
      animated: false,
      backdropDismiss: false,
      cssClass: 'custom-bk-loading',
    })

    await loading.present();

    this.http.get(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos&id=' + idUser).subscribe( data => {
      this.listaParticipacion = data;
      console.log(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos&id=' + idUser );
      console.log(this.listaParticipacion);

     
      
      this.loadingCtrl.dismiss();
    });
  }

  public getParticipacionId(id, tag){
    this.lista_cuotas=[];
    console.log(tag);
    
    let nro_cuotas = tag.datos.length;
    let monto_pagado = tag.monto_total;
    console.log('datos', nro_cuotas, monto_pagado );

    let mont_total = 0;
    let cuotas=0;
    let cant_cuotas=0;
    if (nro_cuotas > 0) {
      mont_total = 3996;
      cuotas = mont_total - monto_pagado;
      cant_cuotas=Math.trunc(this.monto_acumulado/44.4);
      console.log('cant_cuotas 1', cant_cuotas);
    } else {
      mont_total = 3696;
      cuotas = mont_total - monto_pagado;
      cant_cuotas=Math.trunc(this.monto_acumulado/44.4);
      console.log('cant_cuotas 2', cant_cuotas);
    }

    for (let i = 1; i < cant_cuotas+1; i++) {
      this.lista_cuotas.push({val:i, desc: i+ 'Cuotas'});
    }

    // this.lista_cuotas.push({val:'resta', desc: 'Pagar saldo restante '+cuotas});
    console.log(this.lista_cuotas);
    
    this.selectedItem = id;
    console.log(this.selectedItem);
    
    console.log(this.selectedItem);

    this.getMontos(this.selectedItem);
    monto = 3330;
    
  }

  async getMontos(lote){

    const loading = await this.loadingCtrl.create({
      message: 'Verificando información...',
      spinner: "bubbles"
    });

    await loading.present();

    this.http.get(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos_id&id='+lote).subscribe(data => {
      this.participacionData = data;
      
      for(let data of this.participacionData){
        this.monto_por_pagar_url = data.monto;
        let total = data.monto_total;
        
        if(total != 0){
          this.monto_por_pagar = 3996 - parseFloat(total);
        }else{
          this.monto_por_pagar = 3696;
        }
        
        this.participacion = data.descripcion;

        description = this.participacion + ": Pago en cuotas";

        this.loadingCtrl.dismiss();
      }
    });
  }
  public getCuotas(ev:any){
    let cantidad = ev.target.value;
    console.log(cantidad);
    
    tipo= '1';
    monto = 4440 * cantidad;
    description =  this.participacion + ": Pago de "+cantidad+" cuota(s)";
    console.log(monto);

    if(cantidad == 'resta'){
      this.checkedPayRemaining(cantidad);
    }
    this.monto_a_pagar=monto/100;
  }

  async checkedPayRemaining(cantidad){
    var cuota = 4440;
    var cuota2 = '44.4';

      if(this.monto_por_pagar_url == 0){
        tipo= '2';
        monto = this.monto_por_pagar*100;
        description = this.participacion + ": Pago al contado";
      }else{
        tipo= '1';
        monto = this.monto_por_pagar*100;
        description = this.participacion + ": Pago de saldo restante";
      }

    console.log(monto);
  }

  async pagarCuota() {
    if (this.monto_a_pagar > 0) {
      console.log(this.monto_a_pagar);
      console.log(this.monto_a_pagar * 75 / 100);
      const fl = new FormData();
      let monto=this.monto_a_pagar * 75 / 100;
      let monto_para_descontar= this.monto_a_pagar;
      fl.append('iduser',this.idUser);
      console.log(this.selectedItem);
      
      fl.append('idparticipacion',this.selectedItem);
      fl.append('tipo',tipo);
      fl.append('monto_para_descontar',monto_para_descontar+'');
      fl.append('monto',monto+'');

      const loading = await this.loadingCtrl.create(
        {
          spinner: null,
          cssClass: 'custom-eleven-loading',
          backdropDismiss: false
        }
      );
      await loading.present();
  
      this.http.post(this.urI.baseUrl()+'3bk/wallet.php?pagar_con_ganacia='+this.idUser, fl).subscribe(data => {
        const result = JSON.parse(JSON.stringify(data || null ));
        const mensaje = result.status;
        if (mensaje == "success"){
          console.log('ok');
          this.nonoptionSelected();
          this.monto_acumulado=(this.monto_acumulado-monto_para_descontar).toFixed(2);;
          console.log(this.monto_acumulado, monto_para_descontar);
          
          this.selectedItem = '';
          this.lista_cuotas = [];
          this.monto_a_pagar = 0;
          this.makeText('El pago de su cuota se ha realizado con éxito');
          this.router.navigate(['menu/negocio/triplebank/tabs/tabs/bkhome']);
        } else {
          this.makeText(mensaje);
        }
        this.loadingCtrl.dismiss();
        }, err => {
          this.loadingCtrl.dismiss();
          console.log(err);
      })




    }
  }

  // fin

  async makeText(mensaje:string){
    const toast = await this.toastCtrl.create(
      {
        message: mensaje,
        duration: 1500
      }
    )
    await toast.present()
  }


}

