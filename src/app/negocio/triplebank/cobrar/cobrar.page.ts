import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController, IonInput, AlertController, IonSearchbar, ModalController } from '@ionic/angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { Base64 } from '@ionic-native/base64/ngx';
import { HistorialCreditoPage } from 'src/app/historial-credito/historial-credito.page';
import { IndexService } from 'src/app/service/index.service';


@Component({
  selector: 'app-cobrar',
  templateUrl: './cobrar.page.html',
  styleUrls: ['./cobrar.page.scss'],
})
export class CobrarPage implements OnInit {

  @ViewChild('detraccion') Idetraccion ;

  public idUser:string="";
  public ganancia_neta;
  public ganancia_bruta;
  public descuento3bk;
  public descuentopaypal;
  public resta1;
  public resta2;
  public notification;

  public banco:string="";
  public cta:string="";
  public cci:string="";
  public cobrar;
  public arrayData:any;
 
  //To Check
  Inputcheck1:string="";Inputcheck2:string="";Inputcheck3:string="";Inputcheck4:string="";

  //CCI
  entidad:string="";oficina:string="";nro_cuenta:string="";dc:string="";

  //parametros
  public file:string="";
  public filename:string="";
  public extension:string="";
  public tipo:string="";
  public razon_social:string="";
  public ruc:string="";
  public fecha_fac:string="";
  public serie:string="";
  public factura:string=""
  public nro_detraccion:string="";
  // public exonera:boolean=false;
  // public exonerado:string="";
  // public codigo:string="";

  public monto_mayor:string="";

  public detail="0";
  detailIcon="add";

  constructor(public router: Router, private transfer: FileTransfer, private storage: Storage, public loadingCtrl: LoadingController, 
    public toastCtrl: ToastController, public fileChooser: FileChooser, public http: HttpClient, private filePath: FilePath,public urI: IndexService, 
    private base64: Base64, public alertCtrl: AlertController, public modalCtrl: ModalController) { 
      // this.exonera = false;
      // this.exonerado = "0";
      this.monto_mayor="0";
  }

  ngOnInit() {
    this.storage.get('idUserEleven').then(data => {
      this.idUser = data;
    });

    this.storage.get('dniUserEleven').then(dni => {
      this.getIngresos(dni);
    });

    this.storage.get('idUserEleven').then(data => {
      this.counter(data);
    });
  }

  // checkExoneracion(){
  //   if(this.exonera == false){
  //     this.exonerado = '0'
  //   }else{
  //     this.exonerado = '1'
  //   }
  // }

  public checkInputEntidad(value:any){
    if(value.length == 3){
      this.Inputcheck1 = 'V';
    }else{
      this.Inputcheck1 = 'F';
    }
  }

  public checkInputOficina(value:any){
    if(value.length == 3){
      this.Inputcheck2 = 'V';
    }else{
      this.Inputcheck2 = 'F';
    }
  }

  public checkInputNroCuenta(value:any){
    if(value.length == 12){
      this.Inputcheck3 = 'V';
    }else{
      this.Inputcheck3 = 'F';
    }
  }

  public checkInputDC(value:any){
    if(value.length == 2){
      this.Inputcheck4 = 'V';
    }else{
      this.Inputcheck4 = 'F';
    }
  }

  public setCuenta(){
    if(this.Inputcheck1 == 'V'){
      if(this.Inputcheck2 == 'V'){
        if(this.Inputcheck3 == 'V'){
          if(this.Inputcheck4 == 'V'){
            this.cci = this.entidad+'-'+this.oficina+'-'+this.nro_cuenta+'-'+this.dc;
            return 1;
          }else{
            this.makeText('Verifique el CCI');
            return 0;
          }
        }else{
          this.makeText('Verifique el CCI');
          return 0;
        }
      }else{
        this.makeText('Verifique el CCI'); 
        return 0;
      }
    }else{
      this.makeText('Verifique el CCI');
      return 0;
    }
  }

  changeListener($event) : void {
    this.file = $event.target.files[0];
  }

  openFile(){
   this.fileChooser.open().then(uri => {
    this.filePath.resolveNativePath(uri)
		.then(file => {
      this.filename = file.substring(file.lastIndexOf('/')+1);
			let filePath: string = file;
			if (file['type'] != 'image') {
      // convert your file in base64 format
				this.base64.encodeFile(filePath).then((base64File: string) => {
          this.file = base64File;
				}, (err) => {
					alert('err'+JSON.stringify(err));
				});
			}else{
        this.file = "data:image/jpeg;base64,"+this.file;
      }
      })
      .catch(err => console.log(err));
    })
    .catch(e => alert('uri'+JSON.stringify(e)));
  }


  public getIngresos(dni){
    this.http.get(this.urI.baseUrl()+"3bk/3bk.php?referidos_pago&dni="+dni).subscribe( (data) => {
      let dataInfo:any = data;
      for(let data of dataInfo){

        this.ganancia_bruta = data.dolares;
        this.ganancia_bruta = parseFloat(this.ganancia_bruta);
        this.ganancia_bruta = (Math.round(this.ganancia_bruta * 100) / 100);

        this.descuento3bk = parseFloat(this.ganancia_bruta);
        this.descuento3bk = ((this.descuento3bk * 10 ) / 100);
        this.descuento3bk = (Math.round(this.descuento3bk * 100) / 100);

        this.resta1 = parseFloat(this.ganancia_bruta) - parseFloat(this.descuento3bk); 
        this.resta1 = (Math.round(this.resta1 * 100) / 100);

        this.descuentopaypal = parseFloat(this.resta1);
        this.descuentopaypal = ((this.descuentopaypal * 15 ) / 100);
        this.descuentopaypal = (Math.round(this.descuentopaypal * 100) / 100);

        this.resta2 = parseFloat(this.resta1) - parseFloat(this.descuentopaypal);
        this.resta2 = (Math.round(this.resta2 * 100) / 100);

        this.ganancia_neta = parseFloat(this.resta2);
        this.ganancia_neta = (Math.round(this.ganancia_neta * 100) / 100);
      }
    });
  }

  public counter(user){  
      this.http.get(this.urI.baseUrl()+'3bk/3bk.php?notificaciones&user='+user).subscribe( data => {
        var listado:any = data;
        for(let d of listado){
          this.notification = d.conteo;
        } 
      })
  }

  public montoSolicitado(){
    if(this.cobrar > parseFloat(this.ganancia_bruta)){
      return 0;
    }else{
      return 1;
    }
  }

  async sendPayment(){

      if(this.setCuenta() == 0){
        this.makeText('Verifique el CCI');
      }else{
        if(this.montoSolicitado() == 0){
          this.makeText('El monto solicitado es mayor a las ganacias percibidas.');
        }else{
          var url = this.urI.baseUrl()+"3bk/user3bk.php?sol_pago";

        // File for Upload
        var targetPath = this.file;
        console.log(targetPath);
        
        var options = {
          fileKey: "file",
          fileName: this.filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params : {'user': this.idUser,
                    'tipo': '2',
                    'razon_social': this.razon_social,
                    'ruc': this.ruc,
                    'fecha_fac':this.fecha_fac,
                    'banco': this.banco,
                    'nro_cuenta': this.cta,
                    'cci': this.cci,
                    'exonera': '',
                    'cod_exonera': '',
                    'monto': this.cobrar,
                    'extension': '',
                    'nro_serie':this.serie,
                    'nro_factura':this.factura,
                    'detraccion':this.nro_detraccion,
                    'namefile': this.filename},
                    // 'data':this.arrayData},
          HttphttpMethod: 'POST'
        };

        const fileTransfer: FileTransferObject = this.transfer.create();

        const loading = await this.loadingCtrl.create(
          {
            spinner: null,
            cssClass: 'custom-bk-loading',
            backdropDismiss: false
          }
        );
        await loading.present();

        // Use the FileTransfer to upload the image
        fileTransfer.upload(targetPath, url, options).then(data => {
          if(data.response == 'success'){

            this.makeText("Su solicitud de cobro ha sido enviada de manera exitosa.");
            loading.dismiss();

          }else{
            loading.dismiss()
            alert(data.response);
          }

          console.log(options);
          
        }, (err) => {
          loading.dismiss()
          this.makeText(err);
        }); 

      }  
      } 
  }

  public showDetail(){
    this.detailIcon = this.detailIcon === 'add' ? 'remove' : 'add';
    this.detail = this.detail === '0' ? '1' : '0';
  }

  async makeText(mensaje){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    });
    await toast.present();
  }

  public goPage(pagina){
    this.router.navigate(['/menu/negocio/triplebank/'+pagina]);
  }

  public validateAmount(){
    if(this.cobrar > parseFloat(this.ganancia_bruta)) {
      alert('El monto solicitado es mayor a las ganacias percibidas.');
      this.cobrar = this.ganancia_bruta;
    }else{
      if(this.cobrar >= 210){
        let detraccion = (this.cobrar * 10) / 100;
        detraccion = (Math.round( detraccion * 100 ) / 100);
        this.showDialog(detraccion);  
      }else{
        this.monto_mayor = '0';
      }
    }
  }

  async showDialog(monto){
    const alert = await this.alertCtrl.create(
      {
        message: 'Su monto estará sujeto a detraccion.'+'\n'
                  + '<b>Detracción : $ '+ monto+'</b>'+'\n'
                  + 'Ya puede ingresar su Nroº Detracción',
        backdropDismiss: false,
        mode: "ios",
        buttons: [{
            text: 'Listo',
            handler: () => {
              this.monto_mayor = '1';
              alert.dismiss();
              setTimeout(() => {
                this.Idetraccion.setFocus();
              },150);
              return false;
            }          
          }
        ]
      });
    
    await alert.present();
  }

  // async historialPago(){
  //   const modal = await this.modalCtrl.create({
  //     component: HistorialCreditoPage,
  //     componentProps: { 
  //       bruto: this.ganancia_bruta,
  //       desc3bk: this.descuento3bk,
  //       descpp: this.descuentopaypal,
  //       neto: this.ganancia_neta
  //     },
  //     mode: 'ios',
  //     cssClass: 'membresiaModal animated fadeInDownBig',
  //     backdropDismiss: true,
  // });

  //   modal.onDidDismiss().then((data) =>{
  //     const monto_solicitado = data.data['monto'];
  //     const arrayData = data.data['arrayData'];
  //     console.log(monto_solicitado);
  //     console.log(arrayData);
  //     this.cobrar = monto_solicitado;
  //     this.arrayData = arrayData;
  //   })

  //   return await modal.present();
  // }


}
