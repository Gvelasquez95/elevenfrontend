import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-negocio',
  templateUrl: './negocio.page.html',
  styleUrls: ['./negocio.page.scss'],
})
export class NegocioPage implements OnInit {

  constructor(public router:Router, public navCtrl: NavController) { }

  ngOnInit() {
  }

  goPage(pagina){
    this.router.navigate([pagina]);
  }

  getbackClose(){
    this.navCtrl.navigateRoot('/menu/home');
  }

}
