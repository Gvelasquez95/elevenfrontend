import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialCreditoPage } from './historial-credito.page';

describe('HistorialCreditoPage', () => {
  let component: HistorialCreditoPage;
  let fixture: ComponentFixture<HistorialCreditoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialCreditoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialCreditoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
