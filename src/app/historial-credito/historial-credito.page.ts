import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { IndexService } from '../service/index.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-historial-credito',
  templateUrl: './historial-credito.page.html',
  styleUrls: ['./historial-credito.page.scss'],
})
export class HistorialCreditoPage implements OnInit {

  public historialPago:any = [];
  public pagoPagos:any = [];

  public selectedPagos:any = [];
  public monto_total:any=0;

  public allBoxes:string="Seleccionar todo";
  isIndeterminate:boolean;
  masterCheck:boolean;

  shownDetail = null;
  public idUser:string="";

  constructor(public modalCtrl: ModalController, public http: HttpClient, public urI:IndexService, private storage: Storage) { }

  ngOnInit() {
    this.storage.get('idUserEleven').then(value => {
      this.idUser = value;
      this.getHistorial(this.idUser);
    })
  }

  public getHistorial(iduser){
    this.http.get(this.urI.baseUrl()+'3bk/cobros.php?mis_montos&user='+iduser).subscribe(data => {
      this.historialPago = data;
      for(let item of this.historialPago){
        var listaPagos = item.pagos;
        for (let i = 0; i < this.historialPago.length; i++) {
          this.historialPago[i].isChecked=false;
        for (let i2 = 0; i2 < listaPagos.length; i2++) {
          listaPagos[i2].isChecked=false;
        }  
      }
      }
      console.log(this.historialPago);
    })
  }

  public selectItem(ev:any, selectedValue : string, selectedMonto:string){
    let totalItems = this.historialPago.filter((data) => {return data.estado_cobro == '1'} ).length;
   // console.log('Lista con status 1 : '+ totalItems);

    let checkedItem = 0;
    this.historialPago.map(obj => {
      if(obj.estado_cobro == '1'){if (obj.isChecked){checkedItem++;}}
    });
    
    //console.log(checkedItem);

    // if(checked > 0 && totalItems > checked){
    //   this.isIndeterminate = true;
    //   this.masterCheck = false;
    if(checkedItem == totalItems){
      this.masterCheck = true;
      this.isIndeterminate = false;
    }

    if (ev.detail.checked) {

        console.log(selectedValue);
        var pagos=[];
        var val = selectedValue
        this.historialPago.findIndex(function(item, i){
          if (item.id==val) {
           // console.log(item, i);
            pagos=item.pagos
            //return item;
          }
          //return item.id === val
        });

       // console.log(pagos);
        
        this.selectedPagos.push({'participacion':selectedValue, "data":pagos });
        this.monto_total = parseFloat(this.monto_total) + parseFloat(selectedMonto);
        this.monto_total = (Math.round(this.monto_total * 100) / 100);
        this.isIndeterminate = true;
    } else {
      if(checkedItem == 0){
        this.selectedPagos = [];
        this.monto_total = 0;
        this.allBoxes = 'Seleccionar todo';
        this.masterCheck = false;
      }else{
        let index = this.removeCheckedItemFromArray(selectedValue);
        this.selectedPagos.splice(index,1);
        this.monto_total = this.monto_total - parseFloat(selectedMonto);
      }
    }
    console.log(this.selectedPagos);
  }

  public removeCheckedItemFromArray(selectedValue : String) {
    return this.selectedPagos.findIndex((value)=>{
      return value === selectedValue;
    })
  }

  public selectAll(ev:any){
    if(ev.detail.checked){
      this.allBoxes = 'Quitar todo';
    }else{
      this.allBoxes = 'Seleccionar todo';
    }
  }

  checkMaster(ev:any) {
    if (ev.detail.checked) {
      this.historialPago.forEach(obj => {
        if(obj.estado_cobro == '1'){
          obj.isChecked = this.masterCheck;
          this.allBoxes = 'Quitar todo';
        }
      });
    }else{
      this.historialPago.forEach(obj => {
        obj.isChecked = this.masterCheck;
        this.allBoxes = 'Seleccionar todo';
      });
    }
  }

  public geAmount(){
    this.modalCtrl.dismiss({monto:this.monto_total, arrayData:this.selectedPagos});
  }

  public toggleDetail(group){
    if (this.isDetailShown(group)) {
      this.shownDetail = null;
    } else {
        this.shownDetail = group;
    }
  }

  public isDetailShown(group) {
    return this.shownDetail === group;
  };

}
