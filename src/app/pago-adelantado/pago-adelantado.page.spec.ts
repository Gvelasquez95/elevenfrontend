import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoAdelantadoPage } from './pago-adelantado.page';

describe('PagoAdelantadoPage', () => {
  let component: PagoAdelantadoPage;
  let fixture: ComponentFixture<PagoAdelantadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoAdelantadoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoAdelantadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
