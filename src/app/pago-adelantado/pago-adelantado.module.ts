import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoAdelantadoPage } from './pago-adelantado.page';

const routes: Routes = [
  {
    path: '',
    component: PagoAdelantadoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagoAdelantadoPage]
})
export class PagoAdelantadoPageModule {}
