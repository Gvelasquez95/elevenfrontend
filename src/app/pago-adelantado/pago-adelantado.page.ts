import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pago-adelantado',
  templateUrl: './pago-adelantado.page.html',
  styleUrls: ['./pago-adelantado.page.scss'],
})
export class PagoAdelantadoPage implements OnInit {

  public listaImages:any = [];

  constructor() { }

  ngOnInit() {
    this.listaImages = [
      {
        img: '../../assets/images/slider1.jpeg'
      },
      {
        img: '../../assets/images/slider2.jpeg'
      },
      {
        img: '../../assets/images/slider3.jpeg'
      },
      {
        img: '../../assets/images/slider4.jpeg'
      },
      {
        img: '../../assets/images/slider5.jpeg'
      }
    ]
  }



}
