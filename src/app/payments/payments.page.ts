import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { PaymentDetailPage } from '../payment-detail/payment-detail.page';
import { IndexService } from '../service/index.service';


@Component({
  selector: 'app-payments',
  templateUrl: './payments.page.html',
  styleUrls: ['./payments.page.scss'],
})
export class PaymentsPage implements OnInit {

  shownDetail = null;
  
  public id:string="";
  public cuotasData:any=[];
  public participacion:string = "";

  public title:string="";
  public monto:string="";

  constructor(public modalCtrl: ModalController,public http: HttpClient, public navParams: NavParams, public loadingCtrl: LoadingController, public urI:IndexService) { }

  ngOnInit() {
    this.id = this.navParams.get('id');
    this.getCuotas(this.id);
  }

  async getCuotas(iduser){

    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-bk-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

    this.http.get(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos&id='+iduser).subscribe(data => {
      this.cuotasData = data;
      console.log(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos&id='+iduser);
      
      console.log(data);
      
      loading.dismiss();
    });
    
  }

  async openFDetail(idlote){
    const modal = await this.modalCtrl.create({
      component: PaymentDetailPage,
      componentProps: { 
        idlote: idlote
      },
      mode: 'ios',
      cssClass: 'membresiaModal animated zoomIn',
      backdropDismiss: false
    });
    return await modal.present();
  }

  public toggleDetail(group){
    if (this.isDetailShown(group)) {
      this.shownDetail = null;
    } else {
        this.shownDetail = group;
    }
  }

  public  isDetailShown(group) {
    return this.shownDetail === group;
  };

  public close(){
    this.modalCtrl.dismiss();
  }
}
