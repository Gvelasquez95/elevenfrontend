import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoadingController, ToastController } from '@ionic/angular';
import { IndexService } from '../service/index.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  public nombre:string="";
  public dni:string="";
  public correo:string="";
  public telefono:string="";

  constructor(public router: Router, public http: HttpClient, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public urI:IndexService) { }

  ngOnInit() {
  }

  goBack(){
    this.router.navigate(['/login']);
  }

  async registerUser(){
    let url = this.urI.baseUrl() + "userCtrl.php?registro";

    let fr = new FormData();
    fr.append('nombre',this.nombre);
    fr.append('dni', this.dni);
    fr.append('clave', this.dni);
    fr.append('telefono', this.telefono);
    fr.append('correo', this.correo);

    console.log(this.dni);
    

    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-eleven-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

    this.http.post(url, fr).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data || null ));
        console.log(result);
      
      let mensaje=result.status;
        console.log(mensaje);
        
      if(mensaje == "success"){
        this.makeText("¡Registro completado exitosamente, Bienvenido a Eleven!");
        this.router.navigate(['/login']);
      }else{
        this.makeText(mensaje);
      }
        this.loadingCtrl.dismiss();
      },err => {
        this.loadingCtrl.dismiss();
      console.log(err);
      });

  }

  async makeText(mensaje){
    let toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    });
    await toast.present();
  }

}
