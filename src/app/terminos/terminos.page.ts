import { Component, OnInit } from '@angular/core';
import { ToastController, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-terminos',
  templateUrl: './terminos.page.html',
  styleUrls: ['./terminos.page.scss'],
})
export class TerminosPage implements OnInit {

  public confirm:number=0;

  constructor(public toastCtrl: ToastController, public modalCtrl: ModalController, private storage: Storage) {
    this.confirm = 0;
  }

  ngOnInit() {
  }

  public agreement(ev:any){
    if(ev.detail.checked){
      console.log(ev.detail.checked);
      this.confirm = 1;
    }else{
      this.confirm = 0;
    }
  }

  public done(){
    this.makeText('Muchas gracias por confiar en 11 Million.');
    this.modalCtrl.dismiss();
    this.storage.set('terms', '1');
  }

  async makeText(mensaje){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2500
    });
    await toast.present();
  }

}
