import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mic-sheet',
  templateUrl: './mic-sheet.component.html',
  styleUrls: ['./mic-sheet.component.scss'],
})
export class MicSheetComponent implements OnInit {

  public listaActions:any = [];

  constructor() { }

  ngOnInit() {
    this.listaActions = [
      {
        text: 'Ir a inicio'
      },
      {
        text: 'Ir a 3 B'
      }
    ]
  }

}
