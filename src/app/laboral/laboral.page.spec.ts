import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaboralPage } from './laboral.page';

describe('LaboralPage', () => {
  let component: LaboralPage;
  let fixture: ComponentFixture<LaboralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaboralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaboralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
