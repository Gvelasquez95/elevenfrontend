import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-laboral',
  templateUrl: './laboral.page.html',
  styleUrls: ['./laboral.page.scss'],
})
export class LaboralPage implements OnInit {

  public options = "finder";

  constructor(public router: Router) { }

  ngOnInit() {
  }

  public golaboralProfile(){
    this.router.navigate(['']);
  }

}
