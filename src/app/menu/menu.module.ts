import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', loadChildren: '../home/home.module#HomePageModule' },
      { path: 'negocio', loadChildren: '../negocio/negocio.module#NegocioPageModule' },
      { path: 'negocio/triplebank', loadChildren: '../negocio/triplebank/triplebank.module#TriplebankPageModule' },

      { path: 'negocio/triplebank/tabs', loadChildren: '../negocio/triplebank/tabs/tabs.module#TabsPageModule' },

      { path: 'chat', loadChildren: '../chat/chat.module#ChatPageModule' },
      { path: 'chat/detalle', loadChildren: '../chat/detalle/detalle.module#DetallePageModule' },
      { path: 'chat/group', loadChildren: '../chat/group/group.module#GroupPageModule' },

      { path: 'laboral', loadChildren: '../laboral/laboral.module#LaboralPageModule' },
      { path: 'merch', loadChildren: '../merch/merch.module#MerchPageModule' },
      { path: 'voluntario', loadChildren: '../voluntario/voluntario.module#VoluntarioPageModule' },
      { path: 'perfil', loadChildren: '../perfil/perfil.module#PerfilPageModule' },
      { path: 'config', loadChildren: '../config/config.module#ConfigPageModule' },
      { path: 'evento', loadChildren: '../evento/evento.module#EventoPageModule' }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
