import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ToastController, Events, MenuController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Router } from '@angular/router';
import { IndexService } from '../service/index.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  selectedPath = '';

  pages = [];
  arrowfunc: string = 'arrow-down';

  // pages1 = [
  //   {
  //     title: 'Home',
  //     url : '/menu/home',
  //     icon: 'home'
  //   },
  //   {
  //     title: 'Negocios',
  //     url : '/menu/negocio',
  //     icon: 'briefcase'
  //   }
  // ]

  pages1 = [
    {
      title: 'Home',
      url : '/menu/home',
      icon: 'home'
    }
    // {
    //   title: 'Negocios',
    //   url : '/menu/negocio',
    //   icon: 'briefcase'
    // },
    // {
    //   title: 'Chats',
    //   url : '/menu/chat',
    //   icon: 'chatbubbles'
    // },
    // {
    //   title: 'Eventos',
    //   url : '/menu/evento',
    //   icon: 'calendar'
    // },
    // {
    //   title: 'Laboral',
    //   url : '/menu/laboral',
    //   icon: 'construct'
    // },
    // {
    //   title: 'Merchandasing',
    //   url : '/menu/merch',
    //   icon: 'cart'
    // },
    // {
    //   title: 'Voluntariado',
    //   url : '/menu/voluntario',
    //   icon: 'heart'
    // }
  ]

  pages2 = [
    // {
    //   title: 'Perfil',
    //   url : '/menu/perfil',
    //   icon: 'contact'
    // },
    // {
    //   title: 'ElevenCard',
    //   url : '/menu/events',
    //   icon: 'qr-scanner'
    // },
    // {
    //   title: 'Beneficios',
    //   url : '/menu/events',
    //   icon: 'trending-up'
    // },
    // {
    //   title: 'Configuración',
    //   url : '/menu/config',
    //   icon: 'cog'
    // }
  ]

  usuario;
  clave;

  fotoUser;
  nameUser;
  emailUser;
  friends;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public toastCtrl: ToastController, private storage: Storage, 
    public events: Events, public http: HttpClient, private socialSharing: SocialSharing, private menu: MenuController, public router: Router,public urI: IndexService){

  }

  changeOptions() {
    this.arrowfunc = this.arrowfunc === 'arrow-down' ? 'arrow-up' : 'arrow-down';
    if(this.arrowfunc == 'arrow-down'){
      this.pages = this.pages1;
    }else{
      this.pages = this.pages2;
    }
  }
  
  ngOnInit() {
    this.events.subscribe('fillMenu', () => {
      this.storage.get('user').then(u => {
        this.usuario = u;
        this.storage.get('pass').then(p => {
          this.clave = p;
            this.getDataUser(this.usuario,this.clave);
      })
    })
  })
    
    this.pages = this.pages1;
  }

  public getDataUser(user:string,pass:string){
    this.http.get(this.urI.baseUrl()+"userCtrl.php?login&user="+user+"&clave="+pass).subscribe( (data) => {
      let datainfo:any = data;
      for(let datos of datainfo){   
          this.fotoUser = datos.foto;
          this.nameUser = datos.fullname;
          this.emailUser = datos.email;
      }
    });
  }

  private shareWith(){
    let mensaje = "¿Ya eres parte de eleven?, Se parte de nuestra gran comunidad, que tiene como objetivo mejorar a nuestra nación."
    this.socialSharing.share(mensaje,null,null,'https://web.facebook.com/Elevenmillionproject/?ref=br_rs').then(()=>{
    }).catch((err)=>{
        console.log(err)
    });  
  }

  public restartMenu(){
    if(this.arrowfunc == 'arrow-up'){
      this.arrowfunc = 'arrow-down';
      this.pages = this.pages1;
    }
  }

  private async logout(){
    const alert = await this.alertCtrl.create({
      header: 'Cerrar Sesión',
      message: '¿Esta seguro que desea cerrar sesión?',
      backdropDismiss: false,
      mode: 'ios',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            alert.dismiss();
          }
        }, {
          text: 'Salir',
          role: 'destructive',
          handler: () => {
            this.navCtrl.navigateBack('/login');
            this.makeText('¡Hasta pronto!');
            this.storage.remove('idUserEleven');
            this.storage.remove('idUserEleven');
            this.storage.remove('isChecked');
            this.storage.set('idUserEleven','');
            this.storage.set('idUserEleven','');
            
          }
        }
      ]
    });

    await alert.present();
  }

  private async makeText(mensaje){
    let toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 1500,
    })

    await toast.present();
  }

  public goPage(pagina){
    this.router.navigate([pagina]);
    this.menu.close();
  }
}
