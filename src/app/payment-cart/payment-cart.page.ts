import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { CulqiService } from '../service/culqi.service';
import { ExtrasService } from '../service/extras.service';
import { WalletModalPage } from '../wallet-modal/wallet-modal.page';
import { IndexService } from '../service/index.service';
import { Router } from '@angular/router';

declare var Culqi;
declare var window: any;
var data:any={};
var monto;
var description;
var tipo;

@Component({
  selector: 'app-payment-cart',
  templateUrl: './payment-cart.page.html',
  styleUrls: ['./payment-cart.page.scss'],
})
export class PaymentCartPage implements OnInit {

  public participacionData:any=[];
  public listaParticipacion:any = [];
  public ListaItems:any = [];
  public selectedItem:string="";

  public lista_cuotas:any=[];
  public idUser:string="";
  //Parametros
  public participacion:string="";
  public monto_por_pagar:number=0;
  public monto_por_pagar_url:number=1;
  public monto_a_pagar:number=0;
  public fecha_vence;
  public fecha_ultimo_pago;
  public checkedPay:boolean=false;
  public fecha_ultim_pago='';
  constructor(public modalCtrl: ModalController, public http: HttpClient, public loadingCtrl: LoadingController, private storage: Storage, private culqiS: CulqiService, public extras: ExtrasService, public urI:IndexService, public router: Router) {

    this.storage.get('idUserEleven').then(value => {
      this.idUser = value;
      this.getListaParticipacion(this.idUser);
    })

    Culqi.publicKey = 'pk_live_sNNQhairJVASceqd';
    //Culqi.publicKey = 'pk_test_TgbjTFVwQIPhohG2';
    // Configura tu Culqi Checkout

    Culqi.settings({
      title: '3BK',
      currency: 'USD',
      description: description,
      amount: monto,
    });

    Culqi.options({
      style: {
        logo: 'https://www.3bk.pe/img/logo.png'
      }
  });

    window.onCulqiFire = () => {
      if (Culqi.token) { // ¡Objeto Token creado exitosamente!
        var token = Culqi.token.id;

        data.token=Culqi.token.id;
        data.email=Culqi.token.email;
        data.precio=monto;
        data.user=this.idUser;
        data.tipo=tipo;
        data.participacion=this.selectedItem;
        data.moneda='1';
        data.monto=monto/100;
        console.log(data);
        this.extras.loadingCharge('');
        this.culqiS.create({data:data}).subscribe(
          (save:any) => {
            if(save.status == "success"){
              this.extras.simpleAlert('Pago exitoso:','',save.message);
            }else{
              this.extras.simpleAlert('Ocurrió un error:','',save.message);
            }
            this.loadingCtrl.dismiss();
          }, (err) => {
            console.log(err);
            this.loadingCtrl.dismiss();
          }
        )
      } else { // ¡Hubo algún problema!
        // Mostramos JSON de objeto error en consola
        console.log(Culqi.error);
        alert(Culqi.error.user_message);
      }
    }

  }

  // pagarGanancias(){
  //   console.log('monto', monto/100);
      
    
  // }

  open() {
    if (monto!=0) {
      Culqi.settings({
        title: '3BK',
        currency: 'USD',
        description: description,
        amount: monto,
      });
      Culqi.open();
    }else{
      
    }
   
  }

  culqi(){

  }

  ngOnInit() {
    
  }

  public close(){
    this.modalCtrl.dismiss();
  }

  async getListaParticipacion(idUser){

    const loading = await this.loadingCtrl.create({
      animated: false,
      backdropDismiss: false,
      cssClass: 'custom-bk-loading',
    })

    await loading.present();

    this.http.get(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos&id=' + idUser).subscribe( data => {
      this.listaParticipacion = data;
      console.log(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos&id=' + idUser );
      console.log(this.listaParticipacion);

     
      
      this.loadingCtrl.dismiss();
    });
  }

  public getParticipacionId(id, tag){
    this.lista_cuotas=[];
    console.log(tag);
    
    let nro_cuotas = tag.datos.length;
    let monto_pagado = tag.monto_total;
    console.log('datos', nro_cuotas, monto_pagado );

    let mont_total = 0;
    let cuotas=0;
    let cant_cuotas=0;
    if (nro_cuotas > 0) {
      mont_total = 3996;
      cuotas = mont_total - monto_pagado;
      cant_cuotas=Math.trunc(cuotas/33.3);
      console.log('cant_cuotas 1', cant_cuotas);
    } else {
      mont_total = 3696;
      cuotas = mont_total - monto_pagado;
      cant_cuotas=Math.trunc(cuotas/33.3);
      console.log('cant_cuotas 2', cant_cuotas);
    }

    for (let i = 1; i < cant_cuotas+1; i++) {
      this.lista_cuotas.push({val:i, desc: i+ 'Cuotas'});
    }

    this.lista_cuotas.push({val:'resta', desc: 'Pagar saldo restante '+cuotas});
    console.log(this.lista_cuotas);
    
    this.selectedItem = id;
    console.log(this.selectedItem);

    this.getMontos(this.selectedItem);
    monto = 3330;
  }

  async getMontos(lote){

    const loading = await this.loadingCtrl.create({
      message: 'Verificando información...',
      spinner: "bubbles"
    });

    await loading.present();

    this.http.get(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos_id&id='+lote).subscribe(data => {
      this.participacionData = data;
      
      for(let data of this.participacionData){
        this.monto_por_pagar_url = data.monto;
        let total = data.monto_total;
        
        if(total != 0){
          this.monto_por_pagar = 3996 - parseFloat(total);
        }else{
          this.monto_por_pagar = 3696;
        }
        
        this.participacion = data.descripcion;

        description = this.participacion + ": Pago en cuotas";

        this.loadingCtrl.dismiss();
      }
    });
  }

  async checkedPayRemaining(cantidad){
    var cuota = 3330;
    var cuota2 = '33.30';

      if(this.monto_por_pagar_url == 0){
        tipo= '2';
        monto = this.monto_por_pagar*100;
        description = this.participacion + ": Pago al contado";
      }else{
        tipo= '1';
        monto = this.monto_por_pagar*100;
        description = this.participacion + ": Pago de saldo restante";
      }

    console.log(monto);
  }

  public getCuotas(ev:any){
    let cantidad = ev.target.value;
    console.log(cantidad);
    
    tipo= '1';
    monto = 3330 * cantidad;
    description =  this.participacion + ": Pago de "+cantidad+" cuota(s)";
    console.log(monto);

    if(cantidad == 'resta'){
      this.checkedPayRemaining(cantidad);
    }
    this.monto_a_pagar=monto/100;
  }

   goWallet(){
    this.modalCtrl.dismiss();
    this.router.navigate(['/menu/negocio/triplebank/tabs/tabs/billetera']);
    // const modal = await this.modalCtrl.create({
    //   component: WalletModalPage,
    //   componentProps: { 
    //     idUser: this.idUser
    //   },
    //   mode: 'ios',
    //   cssClass: 'walletModal animated fadeInLeft'
    // });
    // return await modal.present();
  }

}
