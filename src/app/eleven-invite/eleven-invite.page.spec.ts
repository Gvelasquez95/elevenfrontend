import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElevenInvitePage } from './eleven-invite.page';

describe('ElevenInvitePage', () => {
  let component: ElevenInvitePage;
  let fixture: ComponentFixture<ElevenInvitePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElevenInvitePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElevenInvitePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
