import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController, ToastController, NavParams } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { IndexService } from '../service/index.service';

@Component({
  selector: 'app-eleven-invite',
  templateUrl: './eleven-invite.page.html',
  styleUrls: ['./eleven-invite.page.scss'],
})
export class ElevenInvitePage implements OnInit {

  public nombre:string="";
  public apellido:string="";
  public dni:string="";
  public celular:string="";
  public correo:string="";
  public iduser:string="";

  constructor(public modalCtrl: ModalController, public http: HttpClient, public loadingCtrl: LoadingController,
    public toastCtrl:ToastController, public navParams: NavParams, public url: IndexService) {
  }

  ngOnInit() {
    this.iduser = this.navParams.data.idUser;
    console.log("invitar: "+this.iduser);
    
  }

  async registerInvitado(){
    var url=this.url+"/3bk/user3bk.php?invitado_reg";

    var fr = new FormData();
    fr.append('nombre',this.nombre);
    fr.append('apellido', this.apellido);
    fr.append('dni', this.dni);
    fr.append('telefono', this.celular);
    fr.append('correo', this.correo);
    fr.append('iduser', this.iduser);
    
    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-eleven-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

      this.http.post(url, fr).subscribe(data => {
      let result = JSON.parse(JSON.stringify(data || null ));
        console.log(result);
      
      let mensaje=result.status;
        console.log(mensaje);
        
      if(mensaje == "success"){
        this.makeText("Registro completo, su invitado debe acercarse a las oficinas de eleven para poder activar su cuenta.");
        this.modalCtrl.dismiss();
      }else{
        this.makeText(mensaje);
      }
        this.loadingCtrl.dismiss();
      },err => {
        this.loadingCtrl.dismiss();
      console.log(err);
      });
  }

  async makeText(mensaje){
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 3500
    });
    await toast.present();
  }

  close(){
    this.modalCtrl.dismiss();
  }

}
