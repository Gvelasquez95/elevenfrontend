import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ElevenInvitePage } from './eleven-invite.page';

const routes: Routes = [
  {
    path: '',
    component: ElevenInvitePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ElevenInvitePage],
  entryComponents: [
    ElevenInvitePage
  ]
})
export class ElevenInvitePageModule {}
