import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GananciadetailboxPage } from './gananciadetailbox.page';

const routes: Routes = [
  {
    path: '',
    component: GananciadetailboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GananciadetailboxPage]
})
export class GananciadetailboxPageModule {}
