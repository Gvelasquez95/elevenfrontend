import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-gananciadetailbox',
  templateUrl: './gananciadetailbox.page.html',
  styleUrls: ['./gananciadetailbox.page.scss'],
})
export class GananciadetailboxPage implements OnInit {

  public bruto;
  public desc3bk;
  public resta1;
  public descpp;
  public resta2;
  public neto;
  public neto_number;

  public monto_solicitado:number=0;
  public monto_detraccion:number=0.00;
  public resta3:number=0.00;

  constructor(public navParams:NavParams, public modalCtrl: ModalController) { }

  ngOnInit() {
    this.bruto = this.navParams.data.bruto;
    this.bruto = parseFloat(this.bruto);
    this.bruto = (Math.round(this.bruto * 100) / 100);

    this.desc3bk = parseFloat(this.bruto);
    this.desc3bk = ((this.desc3bk * 10 ) / 100);
    this.desc3bk = (Math.round(this.desc3bk * 100) / 100);

    this.resta1 = parseFloat(this.bruto) - parseFloat(this.desc3bk); 
    this.resta1 = (Math.round(this.resta1 * 100) / 100);

    this.descpp = parseFloat(this.resta1);
    this.descpp = ((this.descpp * 15 ) / 100);
    this.descpp = (Math.round(this.descpp * 100) / 100);

    this.resta2 = parseFloat(this.resta1) - parseFloat(this.descpp);
    this.resta2 = (Math.round(this.resta2 * 100) / 100);

    this.neto = parseFloat(this.resta2);
    this.neto = (Math.round(this.neto * 100) / 100);
  }

  public setDetraccion(){
    if(this.monto_solicitado > this.neto_number){
      alert("El monto ingresado es mayor a su ganancia neta.");
      this.monto_detraccion = 0.00;
      this.resta3 = 0.00;
    }else{
      this.monto_detraccion = ((this.monto_solicitado * 10 ) / 100);
      this.monto_detraccion = (Math.round(this.monto_detraccion * 100) / 100);

      this.resta3 = this.monto_solicitado - this.monto_detraccion;
      this.resta3 = (Math.round(this.resta3 * 100) / 100);
    }
  }

  public close(){
    this.modalCtrl.dismiss();
  }

}
