import { Injectable } from '@angular/core';
import { ToastController, ModalController, AlertController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ExtrasService {

  constructor(public toastCtrl: ToastController, public modalCtrl: ModalController, public alertCtrl: AlertController, public loadingCtrl: LoadingController) { }

  async makeText(message:string,duration:any){
    const toast = await this.toastCtrl.create({
      message: message,
      duration: duration,
      mode: 'ios',
      position: 'bottom'
    });
    await toast.present()  
  }

  async simpleAlert(title:string,subtitle:string,message:string){
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: subtitle,
      message: message,
      backdropDismiss: false,
      buttons: ['OK']
    });
    await alert.present();
  }

  async loadingCharge(mensaje:string){
    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-bk-loading',
        backdropDismiss: false,
        message: mensaje
      }
    );
    await loading.present();
  }

}
