import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IndexService } from './index.service';

@Injectable({
  providedIn: 'root'
})
export class CulqiService {

  constructor(public http: HttpClient, public url: IndexService) {}

  create(body) {
    const headers = new HttpHeaders()
      .set('Content-type', 'application/json')
    return this.http.post('https://2.11millionproject.com/culqi/create_charge.php', body, { headers: headers });
  }

}