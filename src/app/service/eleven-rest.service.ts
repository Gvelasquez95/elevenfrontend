import { Injectable } from '@angular/core';
import { Observable, of, throwError, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import { map, tap, last, catchError } from 'rxjs/operators';
import { pipe } from '@angular/core/src/render3';
import { post } from 'selenium-webdriver/http';
import { IndexService } from './index.service';

@Injectable({
  providedIn: 'root'
})

export class ElevenRestService {

  public uploadProgress: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public downloadProgress: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient,public urI: IndexService) { }

  // getData(): Observable<any> {
  //   let response1 = this.http.get(apiUrl+'US/00210');
  //   let response2= this.http.get(apiUrl+'IN/110001');
  //   let response3 = this.http.get(apiUrl+'BR/01000-000');
  //   let response4 = this.http.get(apiUrl+'FR/01000');
  //   return forkJoin([response1, response2, response3, response4]);
  // }

  //For Uploading or Downloading
  getStatusMessage(event){

    let status;

    switch(event.type){
      case HttpEventType.Sent:
        return `Uploading Files`;
      
      case HttpEventType.UploadProgress:
        status = Math.round(100 * event.loaded / event.total);
        this.uploadProgress.next(status);
        return `Files are ${status}% uploaded`;

      case HttpEventType.DownloadProgress:
        status = Math.round(100 * event.loaded / event.total);
        this.downloadProgress.next(status); // NOTE: The Content-Length header must be set on the server to calculate this
        return `Files are ${status}% downloaded`; 

      case HttpEventType.Response:
        return `Done`;

      default:
        return `Something went wrong`
    }
  }

  login(formData) : Observable<any>{
    let loginUrl = this.urI.baseUrl()+'userCtrl.php?log';
    
    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    
    return this.http.post(loginUrl,formData,{headers:headers});

    // const requestOptions = new HttpRequest('POST', loginUrl, formData, { 
    //   headers: headers,
    //   // responseType: 'arraybuffer',
    //   // reportProgress: true
    // });

    // return this.http.request(requestOptions).pipe(map(result => {
    //   console.log(result);
    //   })
    // );

    // return this.http.request(requestOptions).pipe(
    //   map(result => {
    //     console.log(result);
    //   })
    // );

    // return this.http.request(requestOptions).pipe(map(event => 
    //   this.getStatusMessage(event)),tap(message => console.log(message)),last());
  }

}
