import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  { path: 'splash', loadChildren: './splash/splash.module#SplashPageModule' },
  { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'forgotpass', loadChildren: './forgotpass/forgotpass.module#ForgotpassPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule'},
  { path: 'payments', loadChildren: './payments/payments.module#PaymentsPageModule' },
  { path: 'payment-detail', loadChildren: './payment-detail/payment-detail.module#PaymentDetailPageModule' },
  { path: 'gananciadetailbox', loadChildren: './gananciadetailbox/gananciadetailbox.module#GananciadetailboxPageModule' },
  { path: 'notification-detail', loadChildren: './notification-detail/notification-detail.module#NotificationDetailPageModule' },
  { path: 'pago-adelantado', loadChildren: './pago-adelantado/pago-adelantado.module#PagoAdelantadoPageModule' },
  { path: 'participacion-cliente', loadChildren: './participacion-cliente/participacion-cliente.module#ParticipacionClientePageModule' },
  { path: 'historial-credito', loadChildren: './historial-credito/historial-credito.module#HistorialCreditoPageModule' },
  { path: 'terminos', loadChildren: './terminos/terminos.module#TerminosPageModule' },
  { path: 'resumen-credito', loadChildren: './resumen-credito/resumen-credito.module#ResumenCreditoPageModule' },
  { path: 'payment-cart', loadChildren: './payment-cart/payment-cart.module#PaymentCartPageModule' },
  { path: 'wallet-modal', loadChildren: './wallet-modal/wallet-modal.module#WalletModalPageModule' },

];

@NgModule({
  providers: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
