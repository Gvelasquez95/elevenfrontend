import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { IndexService } from '../service/index.service';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.page.html',
  styleUrls: ['./payment-detail.page.scss'],
})
export class PaymentDetailPage implements OnInit {

  shownDetail = null;
  arrowfunc: string = 'arrow-dropup';

  public idLote:string="";
  public cuotasData:any=[];
  public participacion:string = "";

  public title:string="";
  public monto:string="";

  constructor(public modalCtrl: ModalController, public navParams: NavParams, public loadingCtrl: LoadingController, public http: HttpClient, public urI:IndexService) { }

  ngOnInit() {
    this.idLote = this.navParams.get('idlote');
    this.getCuotas(this.idLote);
  }

  async getCuotas(idLote){

    const loading = await this.loadingCtrl.create(
      {
        spinner: null,
        cssClass: 'custom-bk-loading',
        backdropDismiss: false
      }
    );
    await loading.present();

    this.http.get(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos_id&id='+idLote).subscribe(data => {
      this.cuotasData = data;
      // console.log(this.urI.baseUrl()+'3bk/user3bk.php?mis_pagos_id&id='+idLote);
      // console.log(this.cuotasData);
      // console.log(this.cuotasData[0].datos);
      
      loading.dismiss();
    });
  }

  showDetail() {
    this.arrowfunc = this.arrowfunc === 'arrow-dropup' ? 'arrow-dropdown' : 'arrow-dropup';
  }

  public close(){
    this.modalCtrl.dismiss();
  }
}
