import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletModalPage } from './wallet-modal.page';

describe('WalletModalPage', () => {
  let component: WalletModalPage;
  let fixture: ComponentFixture<WalletModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WalletModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
