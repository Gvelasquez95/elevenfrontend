import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { IndexService } from '../service/index.service';


@Component({
  selector: 'app-wallet-modal',
  templateUrl: './wallet-modal.page.html',
  styleUrls: ['./wallet-modal.page.scss'],
})

export class WalletModalPage implements OnInit {

  selectedOption:any;
  optionView:any;
  SubTitle:string="";
  estado:string=""; 

  public historialPago:any = [];
  public pagoPagos:any = [];

  public selectedPagos:any = [];
  public monto_total:any=0;

  public allBoxes:string="Todo";
  isIndeterminate:boolean;
  masterCheck:boolean;

  shownDetail = null;
  public idUser:string="";

  constructor(public modalCtrl: ModalController, private storage: Storage, public http: HttpClient, public urI: IndexService) { }

  ngOnInit() {
    this.selectedOption = 0;
    this.estado ="2"; 

    this.storage.get('idUserEleven').then(value => {
      this.idUser = value;
      this.getHistorial(this.idUser);
    })
  }

  public getHistorial(iduser){
    this.http.get(this.urI.baseUrl()+'3bk/cobros.php?lista_montos_wallet&user='+iduser).subscribe(data => {
      this.historialPago = data;
      for(let item of this.historialPago){
        var listaPagos = item.pagos;
        for (let i = 0; i < this.historialPago.length; i++) {
          this.historialPago[i].isChecked=false;
        for (let i2 = 0; i2 < listaPagos.length; i2++) {
          listaPagos[i2].isChecked=false;
        }  
      }
      }
      console.log(this.historialPago);
    })
  }

  public selectItem(ev:any, selectedValue : string, selectedMonto:string){
    let totalItems = this.historialPago.filter((data) => {return data.estado_cobro == '1'} ).length;
   // console.log('Lista con status 1 : '+ totalItems);

    let checkedItem = 0;
    this.historialPago.map(obj => {
      if(obj.estado_cobro == '1'){if (obj.isChecked){checkedItem++;}}
    });
    
    //console.log(checkedItem);

    // if(checked > 0 && totalItems > checked){
    //   this.isIndeterminate = true;
    //   this.masterCheck = false;
    if(checkedItem == totalItems){
      this.masterCheck = true;
      this.isIndeterminate = false;
    }

    if (ev.detail.checked) {

        console.log(selectedValue);
        var pagos=[];
        var val = selectedValue
        this.historialPago.findIndex(function(item, i){
          if (item.id==val) {
           // console.log(item, i);
            pagos=item.pagos
            //return item;
          }
          //return item.id === val
        });

       // console.log(pagos);
        
        this.selectedPagos.push({'participacion':selectedValue, "data":pagos });
        this.monto_total = parseFloat(this.monto_total) + parseFloat(selectedMonto);
        this.monto_total = (Math.round(this.monto_total * 100) / 100);
        this.isIndeterminate = true;
    } else {
      if(checkedItem == 0){
        this.selectedPagos = [];
        this.monto_total = 0;
        this.allBoxes = 'Seleccionar todo';
        this.masterCheck = false;
      }else{
        let index = this.removeCheckedItemFromArray(selectedValue);
        this.selectedPagos.splice(index,1);
        this.monto_total = this.monto_total - parseFloat(selectedMonto);
      }
    }
    console.log(this.selectedPagos);
  }

  public removeCheckedItemFromArray(selectedValue : String) {
    return this.selectedPagos.findIndex((value)=>{
      return value === selectedValue;
    })
  }

  public selectAll(ev:any){
    if(ev.detail.checked){
      this.allBoxes = 'Quitar todo';
    }else{
      this.allBoxes = 'Seleccionar todo';
    }
  }

  checkMaster(ev:any) {
    if (ev.detail.checked) {
      this.historialPago.forEach(obj => {
        if(obj.estado_cobro == '1'){
          obj.isChecked = this.masterCheck;
          this.allBoxes = 'Quitar todo';
        }
      });
    }else{
      this.historialPago.forEach(obj => {
        obj.isChecked = this.masterCheck;
        this.allBoxes = 'Seleccionar todo';
      });
    }
  }

  public geAmount(){
    this.modalCtrl.dismiss({monto:this.monto_total, arrayData:this.selectedPagos});
  }

  public toggleDetail(group){
    if (this.isDetailShown(group)) {
      this.shownDetail = null;
    } else {
        this.shownDetail = group;
    }
  }

  public isDetailShown(group) {
    return this.shownDetail === group;
  };

  optionSelected(value){
    this.selectedOption = 1;

    if(value == 1){
      this.optionView = value;
      this.SubTitle = 'Historial'; 
    }else if(value == 2){
      this.optionView = value;
      this.SubTitle = 'Billetera'; 
    }else{
      this.optionView = value;
      this.SubTitle = 'Pagar participación'; 
    }
  }

  nonoptionSelected(){
    this.selectedOption = 0;
    this.optionView = '';
    this.SubTitle = ''; 
  }

  public getBack(){
    this.modalCtrl.dismiss();
  }

}
