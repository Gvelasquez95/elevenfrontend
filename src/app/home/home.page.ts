import { Component, OnInit } from '@angular/core';
import { MenuController, Events, PopoverController, ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { MicSheetComponent } from '../mic-sheet/mic-sheet.component';
import { Router } from '@angular/router';
import { ElevenInvitePage } from '../eleven-invite/eleven-invite.page';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public idUser="";
  public dniUser="";

  //BO3 Chats
  public listaChats:any = [];
  public listaPosts:any = [];

  colorRecord = 'dark';
  micIcon = 'mic';
  expression: any = '';
  titleEvent = 'Evento 1';
  days = '1';
  constructor(public menu: MenuController, public http: HttpClient, private storage: Storage, public events: Events,public modalCtrl: ModalController,
    private sR: SpeechRecognition, public popoverCtrl: PopoverController, public router: Router) {}

  ionViewDidEnter(){
    this.menu.enable(true);
  }

  ngOnInit(){

    this.storage.get('idUserEleven').then(id => {
      this.idUser = id;
      console.log("idUser: "+id);
    });

    this.storage.get('dniUserEleven').then(dni => {
      console.log("dniUser: "+dni);
    });

    this.storage.get('user').then(u => {
      console.log("user: "+u);
    });

    this.storage.get('pass').then(p => {
      console.log("pass: "+p);
    });

    this.events.publish('fillMenu');

  }

  public voiceRecognition(){

    this.checkVoicePermission();
    this.requestVoicePermission();

    let options = {
      language: 'es-PE'
    }

      this.sR.startListening(options).subscribe((matches: string[]) => {
        this.expression = matches;
  
        if(this.expression == 'ir a inicio' || this.expression == 'inicio'){
          this.router.navigate(['/menu/home']);
        }else if(this.expression == 'ir a 3bk' || this.expression == '3bk' || this.expression == 'ir a 3beca' || this.expression == '3beca'){
          this.router.navigate(['/menu/negocio/triplebank']);
        }
      });
  }

  public checkVoicePermission(){
    this.sR.hasPermission().then((hasPermission: boolean) => console.log(hasPermission));
  }

  public requestVoicePermission(){
    this.sR.requestPermission().then(() => console.log('Granted'),() => console.log('Denied'));
  }

  async showInfo(ev: any){
    const popover = await this.popoverCtrl.create({
      component: MicSheetComponent,
      event: ev,
      backdropDismiss: true,
      mode: "ios"
    });
    return await popover.present();
  }

  public getChats(){
    this.http.get('').subscribe(data => {
      this.listaChats = data;
    })
  }

  public getPosts(){
    this.http.get('').subscribe(data => {
      this.listaPosts = data;
    })
  }
  
  async inviteModal(){
    const modal = await this.modalCtrl.create({
      component: ElevenInvitePage,
      componentProps: { 
        idUser: this.idUser
      },
      mode: 'ios',
      cssClass: 'membresiaModal animated flipInY'
    });
    return await modal.present();
  }

  public goPage(pagina){
    this.router.navigate([pagina]);
  }
  
}
 